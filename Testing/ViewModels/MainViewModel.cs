﻿using Shared.Logging;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Testing.Common;

namespace Testing.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        public ICommand ToggleServer { get; }
        public ICommand ClearLogsCommand { get; }
        public ObservableCollection<LogMessage> Logs
        {
            get => Logger.logs;
        }
        
        public MainViewModel()
        {
            ToggleServer = new RelayCommand(ExecuteToggleServer, null);
            ClearLogsCommand = new RelayCommand(ExecuteClearLogs, null);
        }

        private void ExecuteClearLogs()
        {
            Logger.Clear();
        }

        private void ExecuteToggleServer()
        {
            if (IsRunning)
            {
                App.Proxy.Stop();
            } else
            {
                App.Proxy.Start();
            }

            OnPropertyChanged("IsRunning");
        }

        public bool IsRunning
        {
            get => App.Proxy.IsRunning;
        }

        public int Port
        {
            get => App.Config.Port;
            set
            {
                App.Config.Port = value;
                OnPropertyChanged();
            }
        }
        public bool IsLogging
        {
            get => App.Config.IsLoggingEnabled;
            set
            {
                App.Config.IsLoggingEnabled = value;
                OnPropertyChanged();
            }
        }

        public bool IsCaching
        {
            get => App.Config.IsCachingEnabled;
            set
            {
                App.Config.IsCachingEnabled = value;
                OnPropertyChanged();
            }
        }

        public bool IsBasicAuthOn
        {
            get => App.Config.IsBasicAuthEnabled;
            set
            {
                App.Config.IsBasicAuthEnabled = value;
                OnPropertyChanged();
            }
        }

        public bool IsContentBlocking
        {
            get => App.Config.IsContentBlockingEnabled;
            set
            {
                App.Config.IsContentBlockingEnabled = value;
                OnPropertyChanged();
            }
        }

        public bool IsProtectingPrivacy
        {
            get => App.Config.IsPrivacyEnabled;
            set
            {
                App.Config.IsPrivacyEnabled = value;
                OnPropertyChanged();
            }
        }

        public bool IsBlockingHosts
        {
            get => App.Config.IsUrlBlockingEnabled;
            set
            {
                App.Config.IsUrlBlockingEnabled = value;
                OnPropertyChanged();
            }
        }

        public bool IsForcedTimeout
        {
            get => App.Config.IsForcedTimeout;
            set
            {
                App.Config.IsForcedTimeout = value;
                OnPropertyChanged();
            }
        }

        public int ForcedTimeout
        {
            get => App.Config.ForcedTimeout;
            set
            {
                App.Config.ForcedTimeout = value;
                OnPropertyChanged();
            }
        }

        public int BufferSize
        {
            get => App.Config.BufferSize;
            set
            {
                App.Config.BufferSize = value;
                OnPropertyChanged();
            }
        }

    }
}
