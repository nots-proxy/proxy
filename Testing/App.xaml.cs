﻿using Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Testing
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static ProxyConfig Config = new ProxyConfig() {
            BlockList = new List<string>() { "platform.twitter.com", "gstatic.com" }
        };
    public static Proxy Proxy = new Proxy(Config);
    }
}
