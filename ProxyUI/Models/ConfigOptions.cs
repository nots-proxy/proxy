using ProxyUI.ViewModels;
using System.Collections.Generic;

namespace ProxyUI.Models
{
    public sealed class ConfigOptions : BindableBase
    {
        private int port = 0;
        private bool stop;
        private bool isRunning;
        private bool isContentBlockingEnabled;
        private bool isUrlBlockingEnabled;
        private bool isPrivacyEnabled;
        private bool isBasicAuthEnabled;
        private bool isLoggingEnabled;
        private Credentials authCredentials = new Credentials() { Username = "" };
        private bool isCachingEnabled;
        private int bufferSize = 1024;
        private List<string> blockList;

        public int Port { get => port; set => Set(ref port, value); }
        public bool Stop { get => stop; set => Set(ref stop, value); }
        public bool IsRunning { get => isRunning; set => Set(ref isRunning, value); }
        public bool IsContentBlockingEnabled { get => isContentBlockingEnabled; set => Set(ref isContentBlockingEnabled, value); }
        public bool IsUrlBlockingEnabled { get => isUrlBlockingEnabled; set => Set(ref isUrlBlockingEnabled, value); }
        public bool IsPrivacyEnabled { get => isPrivacyEnabled; set => Set(ref isPrivacyEnabled, value); }
        public bool IsBasicAuthEnabled { get => isBasicAuthEnabled; set => Set(ref isBasicAuthEnabled, value); }

        public bool IsLoggingEnabled { get => isLoggingEnabled; set => Set(ref isLoggingEnabled, value); }

        public Credentials AuthCredentials { get => authCredentials; set => Set(ref authCredentials, value); }
        public bool IsCachingEnabled { get => isCachingEnabled; set => Set(ref isCachingEnabled, value); }
        public int BufferSize { get => bufferSize; set => Set(ref bufferSize, value); }
        public List<string> BlockList { get => blockList; set => Set(ref blockList, value); }
    }

    public sealed class Credentials : BindableBase
    {
        private string username;
        private string password;

        public string Username { get => username; set => Set(ref username, value); }
        public string Password { get => password; set => Set(ref password, value); }
    }
}