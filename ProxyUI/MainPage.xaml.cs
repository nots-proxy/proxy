﻿using ProxyUI.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ProxyUI
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void NavView_Loaded(object sender, RoutedEventArgs e)
        {
            NavView_Navigate("settings", new EntranceNavigationTransitionInfo());
        }

        private readonly List<(string Tag, Type Page)> _pages = new List<(string Tag, Type Page)>
        {
            ("auth", typeof(AuthPage)),
            ("cache", typeof(CachingPage)),
            ("filter", typeof(FilterPage)),
        };

        private void OnNavItemInvoked(NavigationView sender, NavigationViewItemInvokedEventArgs args)
        {
            var transition = args.RecommendedNavigationTransitionInfo;
            if (args.IsSettingsInvoked == true)
            {
                NavView_Navigate("settings", transition);
            }

            var item = args.InvokedItemContainer;

            if (item != null && item.Tag != null)
            {
                var itemTag = item.Tag.ToString();
                NavView_Navigate(itemTag, transition);
            }
        }

        private void NavView_Navigate(string navItemTag, NavigationTransitionInfo transitionInfo)
        {
            Type _page = null;
            if (navItemTag == "settings")
            {
                _page = typeof(SettingsPage);
            }
            else
            {
                var item = _pages.FirstOrDefault(p => p.Tag.Equals(navItemTag));
                _page = item.Page;
            }

            var currentPageType = ContentFrame.CurrentSourcePageType;

            if (!(_page is null) && !Type.Equals(currentPageType, _page))
            {
                ContentFrame.Navigate(_page, null, transitionInfo);
            }
        }

        private void On_Navigated(object sender, NavigationEventArgs e)
        {
            if (ContentFrame.SourcePageType == typeof(SettingsPage))
            {
                NavView.SelectedItem = NavView.SettingsItem;
                NavView.Header = "Settings";
            }
            else if (ContentFrame.SourcePageType != null)
            {
                var item = _pages.FirstOrDefault(p => p.Page == e.SourcePageType);

                NavView.SelectedItem = NavView.MenuItems.OfType<NavigationViewItem>().First(n => n.Tag.Equals(item.Tag));

                NavView.Header = ((NavigationViewItem)NavView.SelectedItem)?.Content?.ToString();
            }
        }

        private void ContentFrame_OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception($"Navigation failed {e.Exception.Message} for {e.SourcePageType.FullName}");
        }
    }
}
