using ProxyUI.ViewModels;
using Windows.UI.Xaml.Controls;

namespace ProxyUI.Views
{
    public sealed partial class FilterPage : Page
    {
        public FilterViewModel ViewModel { get; } = new FilterViewModel();
        public FilterPage()
        {
            InitializeComponent();
        }
    }
}
