using ProxyUI.ViewModels;
using Windows.UI.Xaml.Controls;

namespace ProxyUI.Views
{
    public sealed partial class SettingsPage : Page
    {
        public SettingsViewModel ViewModel { get; } = new SettingsViewModel();

        public SettingsPage()
        {
            InitializeComponent();
        }
    }
}
