using ProxyUI.ViewModels;
using Windows.UI.Xaml.Controls;

namespace ProxyUI.Views
{
    public sealed partial class AuthPage : Page
    {
        public AuthViewModel ViewModel { get; } = new AuthViewModel();
        public AuthPage()
        {
            InitializeComponent();
        }
    }
}
