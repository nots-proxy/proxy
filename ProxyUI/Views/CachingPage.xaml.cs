using ProxyUI.ViewModels;
using Windows.UI.Xaml.Controls;

namespace ProxyUI.Views
{
    public sealed partial class CachingPage : Page
    {
        public CachingViewModel ViewModel { get; } = new CachingViewModel();
        public CachingPage()
        {
            InitializeComponent();
        }
    }
}
