namespace ProxyUI.ViewModels
{
    public class CachingViewModel : BindableBase
    {
        public bool IsCaching
        {
            get => App.Config.IsCachingEnabled;
            set
            {
                App.Config.IsCachingEnabled = value;
                OnPropertyChanged();
            }
        }
    }
}
