namespace ProxyUI.ViewModels
{
    public class SettingsViewModel : BindableBase
    {

        private bool _isRunning;
        public bool IsRunning
        {
            get => _isRunning;
            set
            {
                if (value)
                    App.Proxy.Start();
                else
                    App.Proxy.Stop();

                Set(ref _isRunning, value);
            }
        }

        public int Port
        {
            get => App.Config.Port;
            set
            {
                App.Config.Port = value;
                OnPropertyChanged();
            }
        }

        public bool IsLogging
        {
            get => App.Config.IsLoggingEnabled;
            set
            {
                App.Config.IsLoggingEnabled = value;
                OnPropertyChanged();
            }
        }

        public string RenderToggleText(bool? c) => c is bool check && check ? "Stop" : "Run";
    }
}
