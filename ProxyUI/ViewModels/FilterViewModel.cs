using ProxyUI.Common;
using ProxyUI.Models;
using Shared;
using System;
using System.ComponentModel;
using System.Linq;

namespace ProxyUI.ViewModels
{
    public class FilterViewModel : BindableBase
    {
        public RelayCommand SaveHosts;
        public FilterViewModel()
        {
            SaveHosts = new RelayCommand(ExecuteSaveHosts, CanExecuteSaveHosts);
            Hosts = string.Join(Environment.NewLine, App.Config.BlockList);
            App.Config.PropertyChanged += DidHostsChange;
        }

        private string _hosts;
        public string Hosts
        {
            get => _hosts;
            set
            {
                Set(ref _hosts, value);
                SaveHosts.RaiseCanExecuteChanged();
            }
        }

        private void DidHostsChange(object sender, PropertyChangedEventArgs e)
        {
            var config = (ProxyConfig)sender;
            var currentHosts = Hosts.Split(
                "\r",
                StringSplitOptions.RemoveEmptyEntries).ToList();

            if (!Equals(currentHosts, config.BlockList))
            {
                Hosts = string.Join(Environment.NewLine, config.BlockList);
            }
        }

        private void ExecuteSaveHosts()
        {
            // BUG: UWP textbox does not use the environment newline. Only \r.
            var hosts = Hosts.Split(
                "\r",
                StringSplitOptions.RemoveEmptyEntries).ToList();

            App.Config.BlockList = hosts;
        }

        private bool CanExecuteSaveHosts() => !string.IsNullOrEmpty(Hosts);

        public bool IsContentBlocking
        {
            get => App.Config.IsContentBlockingEnabled;
            set
            {
                App.Config.IsContentBlockingEnabled = value;
                OnPropertyChanged();
            }
        }

        public bool IsProtectingPrivacy
        {
            get => App.Config.IsPrivacyEnabled;
            set
            {
                App.Config.IsPrivacyEnabled = value;
                OnPropertyChanged();
            }
        }

        public bool IsBlockingHosts
        {
            get => App.Config.IsUrlBlockingEnabled;
            set
            {
                App.Config.IsUrlBlockingEnabled = value;
                OnPropertyChanged();
            }
        }
    }
}
