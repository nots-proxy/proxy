namespace ProxyUI.ViewModels
{
    public class MainViewModel : ComputedBindableBase
    {
        private bool _isLoading = false;
        public bool IsLoading
        {
            get => _isLoading;
            set => Set(ref _isLoading, value);
        }
    }
}
