using ProxyUI.Models;
using System.Threading.Tasks;
using Windows.Storage.Provider;

namespace ProxyUI.Repository
{
    public interface IConfigOptionsRepository
    {
        Task<ConfigOptions> GetAsync(string filePath);
        ConfigOptions Get(string filePath);
        Task<FileUpdateStatus> UpsertAsync(string path, ConfigOptions configOptions);
    }
}
