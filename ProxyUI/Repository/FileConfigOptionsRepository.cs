using Newtonsoft.Json;
using ProxyUI.Models;
using System;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Provider;

namespace ProxyUI.Repository
{
    class FileConfigOptionsRepository : IConfigOptionsRepository
    {
        public async Task<ConfigOptions> GetAsync(string path)
        {
            var file = await StorageFile.GetFileFromPathAsync(path);
            var jsonString = await FileIO.ReadTextAsync(file);
            return JsonConvert.DeserializeObject<ConfigOptions>(jsonString);
            //DeserializeAsync<ConfigOptions>(jsonString);
        }

        public ConfigOptions Get(string path)
        {
            if (path == null) return new ConfigOptions() { AuthCredentials = new Credentials() };

            //var task = StorageFile.GetFileFromPathAsync(Location);
            var task = Task.Run(async () => await StorageFile.GetFileFromPathAsync(path));
            task.Wait();
            var file = task.Result;
            var readTask = FileIO.ReadTextAsync(file).AsTask();
            readTask.Wait();
            var jsonString = readTask.Result;
            return JsonConvert.DeserializeObject<ConfigOptions>(jsonString);
        }
        public async Task<FileUpdateStatus> UpsertAsync(string path, ConfigOptions configOptions)
        {
            try
            {
                var file = await StorageFile.GetFileFromPathAsync(path);
                CachedFileManager.DeferUpdates(file);

                var jsonString = JsonConvert.SerializeObject(configOptions);
                await FileIO.WriteTextAsync(file, jsonString);

                return await CachedFileManager.CompleteUpdatesAsync(file);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
