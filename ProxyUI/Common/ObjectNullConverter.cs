﻿using System;
using Windows.UI.Xaml.Data;

namespace ProxyUI.Common
{
    class ObjectNullConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return !Equals(value, null);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
    }
}
