﻿using backend.Logger;

namespace backend
{
    class Program
    {
        public static void Main()
        {
            StartProxy();
            ProxyLogger.Log(Level.Info, "Proxy stopped");
        }

        private static void StartProxy()
        {
            var proxy = new Proxy();
            proxy.Listen();
            var config = Config.GetInstance();
            if (config.Restart && !config.Options.Stop)
            {
                Config.GetInstance().Restart = false;
                StartProxy();
            }
        }
    }
}