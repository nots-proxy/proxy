using System.IO;
using System.Net;
using System.Text.Json;
using backend.Logger;

namespace backend
{
    public sealed class Config
    {
        private readonly HttpListener _listener;
        private static Config _instance;
        // watcher needs to be kept in memory
        // ReSharper disable once NotAccessedField.Local
        private readonly ConfigWatcher _watcher;
        public ConfigOptions Options { get; private set; }
        public bool Restart { get; set; }

        private Config(HttpListener listener)
        {
            _listener = listener;
            var jsonString = File.ReadAllText("config.json");
            Options = JsonSerializer.Deserialize<ConfigOptions>(jsonString);
            _watcher = new ConfigWatcher(ParseNewConfig);
        }

        public static Config GetInstance(HttpListener listener) => _instance ??= new Config(listener);
        public static Config GetInstance() => _instance;

        private async void ParseNewConfig(object source, FileSystemEventArgs e)
        {
            var configStream = File.OpenRead("config.json");
            var res = await JsonSerializer.DeserializeAsync<ConfigOptions>(configStream);
            if (res.Port != Options.Port)
            {
                Restart = true;
            }

            Options = res;
            _listener.AuthenticationSchemes = Options.IsBasicAuthEnabled ? AuthenticationSchemes.Basic : AuthenticationSchemes.None;
            ProxyLogger.Log(Level.Info, "Configuration updated");
        }
    }
}