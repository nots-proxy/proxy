# proxy

Nots Proxy

## Design

### flow chart

```mermaid
graph TD

a[Accept incoming message]
b[Fetch request]
c{request in cache}
d{cache still valid}
e[Relay cache back to client]
f{client request only cached}
g[Return invalid cache]
h[Return Gateway timeout]

i[Send external request]
j[Relay external response back to client]
l{External response is cacheable}
k[Store request in cache]


a --> b
b --> c

c -->|No| i
c -->|Yes| d

d -->|No| f
d -->|Yes| e

f -->|No| i
f -->|Yes| h

i --> j
j --> l

l -->|Yes| k

```

### class diagram

```mermaid
classDiagram

class Proxy{
  +constructor(port) void
  +listen() bool
  -HandleContextAsync()
}

```
