using System.Threading.Tasks;
using backend.Caching;
using backend.Filtering;
using backend.Models;
using backend.Privacy;

namespace backend
{
    public class RequestHandler
    {
        public async Task<IResponse> HandleRequest(IListenerRequest request)
        {
            if (Config.GetInstance().Options.IsPrivacyEnabled)
            {
                var privacyManager = new PrivacyManager();
                privacyManager.RemoveClientHeaders(request);
            }
            
            var filterManager = new FilterManager();
            var response = filterManager.Filter(request);
            if (response != null) return response;
            
            var cacheManager = new CacheManager();
            return await cacheManager.GetRequest(request);
        }
    }
}
