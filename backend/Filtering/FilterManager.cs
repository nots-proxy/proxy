using backend.Filtering.Strategy;
using backend.Logger;
using backend.Models;

namespace backend.Filtering
{
    public class FilterManager
    {
        public IResponse Filter(IListenerRequest request)
        {
            if (Config.GetInstance().Options.IsContentBlockingEnabled)
            {
                var contentFilter = new ContentFilterStrategy();
                var cfResponse = contentFilter.Filter(request.Url);
                if (cfResponse != null)
                {
                    ProxyLogger.Log(Level.Info, "Content Blocking", request.RequestTraceIdentifier);
                    return cfResponse;
                }
            }

            if (Config.GetInstance().Options.IsUrlBlockingEnabled)
            {
                var urlFilter = new UrlFilterStrategy();
                var ufResponse = urlFilter.Filter(request.Url);
                if (ufResponse != null)
                {
                    ProxyLogger.Log(Level.Info, "Url blocking", request.RequestTraceIdentifier);
                    return ufResponse;
                }
            }

            return null;
        }
    }
}