using System;
using System.Linq;
using System.Net;
using backend.Exceptions;
using backend.Extensions;
using backend.Models;

namespace backend.Filtering.Strategy
{
    public class UrlFilterStrategy
    {
        public IResponse Filter(Uri uri)
        {
            var blockList = Config.GetInstance().Options.BlockList;
            if (blockList.IsAny() && blockList.Any(s => s == uri.Host))
            {
                throw new WebResponseException(HttpStatusCode.BadGateway, "Url blocked");
            }

            return null;
        }
    }
}