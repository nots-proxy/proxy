using System.Collections.Generic;

// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable ClassNeverInstantiated.Global

namespace backend
{
    public class ConfigOptions
    {
        public int Port { get; set; }
        public bool Stop { get; set; }
        public bool IsContentBlockingEnabled { get; set; }
        public bool IsUrlBlockingEnabled { get; set; }
        public bool IsPrivacyEnabled { get; set; }
        public bool IsBasicAuthEnabled { get; set; }
        
        public bool IsLoggingEnabled { get; set; }

        public class Credentials
        {
            public string Username { get; set; }
            public string Password { get; set; }
        }

        public Credentials AuthCredentials { get; set; }
        public bool IsCachingEnabled { get; set; }
        public int BufferSize { get; set; }
        public List<string> BlockList { get; set; } 
    }
}