using System.IO;

namespace backend
{
    public class ConfigWatcher
    {
        // ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
        private readonly FileSystemWatcher _watcher;
        public ConfigWatcher(FileSystemEventHandler parseNewConfig)
        {
            _watcher = new FileSystemWatcher(Directory.GetCurrentDirectory())
            {
                NotifyFilter = NotifyFilters.FileName | NotifyFilters.LastWrite,
                Filter = "config.json",
                EnableRaisingEvents = true,
                IncludeSubdirectories = true
            };

            _watcher.Changed += parseNewConfig;
        }
    }
}