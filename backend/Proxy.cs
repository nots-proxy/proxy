using System;
using System.Net;
using System.Threading.Tasks;
using backend.Auth;
using backend.Exceptions;
using backend.Logger;
using backend.Models;

namespace backend
{
    internal class Proxy
    {
        private readonly HttpListener _httpListener;
        public Proxy()
        {
            _httpListener = new HttpListener();
            var config = Config.GetInstance(_httpListener);
            // URI prefixes are required,
            var prefix = $"http://*:{config.Options.Port}/";
            _httpListener.Prefixes.Add(prefix);
            _httpListener.AuthenticationSchemes = config.Options.IsBasicAuthEnabled
                ? AuthenticationSchemes.Basic
                : AuthenticationSchemes.None;
            _httpListener.Start();
            ProxyLogger.Log(Level.Info, $"Proxy started at Port: {config.Options.Port}");
        }

        public void Listen()
        {
            var config = Config.GetInstance();
            while (!config.Restart && !config.Options.Stop)
            {
                var context = _httpListener.GetContext();
                Task.Run(() => HandleContextAsync(context));
            }
            _httpListener.Stop();
        }

        private async void HandleContextAsync(HttpListenerContext context)
        {
            var request = new ListenerRequest(context.Request);
            var response = context.Response;
            
            LogIncomingRequest(request);

            if (context.User?.Identity != null)
            {
                var authManager = new AuthManager();
                if (!authManager.IsAuthorized((HttpListenerBasicIdentity) context.User.Identity))
                {
                    authManager.HandleUnauthorized(response);
                    ProxyLogger.Log(Level.Warning, "UnAuthorized", request.RequestTraceIdentifier);
                    return;
                }
                authManager.RemoveAuthHeaders(request);
            }

            var requestHandler = new RequestHandler();
            var responseHandler = new ResponseHandler();

            try
            {
                var externalRes = await requestHandler.HandleRequest(request);
                responseHandler.HandleResponse(response, externalRes, request);
            }
            catch (WebResponseException e)
            {
                await responseHandler.HandleCustomResponseError(response, e, request);
            }
            catch (WebException e)
            {
                await responseHandler.HandleResponseError(response, e, request);
            }
            catch (Exception e)
            {
                ProxyLogger.Log(e, request.RequestTraceIdentifier);
                response.StatusCode = 500;
                response.Close();
            }
        }

        private void LogIncomingRequest(IListenerRequest request)
        {
            var message = $"{request.RawUrl}: {request.Headers}";
            ProxyLogger.Log(Level.Info, message, request.RequestTraceIdentifier);
        }
    }
}