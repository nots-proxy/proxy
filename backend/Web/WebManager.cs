using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using backend.Extensions;
using backend.Logger;
using backend.Models;

namespace backend.Web
{
    public class WebManager : IWebManager
    {
        public async Task<HttpWebRequest> BuildWebRequest(IListenerRequest request)
        {
            ProxyLogger.Log(Level.Info, "Building new request", request.RequestTraceIdentifier);
            var http = WebRequest.CreateHttp(request.RawUrl);
            http.Method = request.HttpMethod;
            http.UserAgent = request.UserAgent;
            http.AddHeaders(request.Headers);

            if (request.ContentLength64 <= 0) return http;
            
            var httpRequestStream = http.GetRequestStream();
            await request.InputStream.CopyToAsync(httpRequestStream);

            return http;
        }

        public IRequest BuildValidateRequest(IResponse response)
        {
            var request = new Request(response.ResponseUri.ToString());
            request.AddHttpMethod(HttpMethod.Get);
            request.AddHeader(HttpRequestHeader.IfModifiedSince, response.Headers.Get("Date"));
            return request;
        }
    }
}