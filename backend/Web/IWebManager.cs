using System.Net;
using System.Threading.Tasks;
using backend.Models;

namespace backend.Web
{
    public interface IWebManager
    {
        public Task<HttpWebRequest> BuildWebRequest(IListenerRequest request);
        public IRequest BuildValidateRequest(IResponse requestResponse);
    }
}