namespace backend.Logger
{
    public enum Level
    {
        None,
        Debug,
        Info,
        Warning,
        Error
    }
}