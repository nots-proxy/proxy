using System;
using System.Collections.Concurrent;
using System.Threading;

namespace backend.Logger
{
    public static class ProxyLogger
    {
        private static readonly ConcurrentQueue<LogMessage> LogQueue = new ConcurrentQueue<LogMessage>();

        static ProxyLogger()
        {
            var thread = new Thread(
                () =>
                {
                    while (true)
                    {
                        if (!LogQueue.TryDequeue(out var logMessage)) continue;
                        if (Config.GetInstance().Options.IsLoggingEnabled)
                        {
                            Console.WriteLine(logMessage);
                        }
                    }
                    // ReSharper disable once FunctionNeverReturns
                }) {IsBackground = true};
            thread.Start();
        }
        
        public static void Log(Exception exception)
        {
            Log(Level.Error, exception.Message);
        }
        
        public static void Log(Exception exception, Guid traceId)
        {
            Log(Level.Error, exception.Message, traceId);
        }
        
        public static void Log(Level level, string message)
        {
            Log(level, message, DateTime.Now);
        }
        
        public static void Log(Level level, string message, Guid traceId)
        {
            Log(level, message, DateTime.Now, traceId);
        }
        
        public static void Log(Level level, string message, DateTime dateTime)
        {
            var logObject = new LogMessage(level, message, dateTime);
            LogQueue.Enqueue(logObject);
        }

        public static void Log(Level level, string message, DateTime dateTime, Guid traceId)
        {
            var logObject = new LogMessage(level, message, dateTime, traceId);
            LogQueue.Enqueue(logObject);
        }
    }
}