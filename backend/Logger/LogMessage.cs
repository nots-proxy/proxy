using System;

namespace backend.Logger
{
    public class LogMessage
    {
        public LogMessage(Level level, string text, DateTime dateTime)
        {
            Level = level;
            Text = text;
            DateTime = dateTime;
        }

        public LogMessage(Level level, string text, DateTime dateTime,  Guid traceId)
        {
            DateTime = dateTime;
            Level = level;
            Text = text;
            TraceId = traceId;
        }

        public DateTime DateTime { get; set; }
        public Level Level { get; set; }
        public string Text { get; set; }
        
        public Guid? TraceId { get; set; }

        public override string ToString()
        {
            return new LoggerFormatter().ApplyFormat(this);
        }
    }
}