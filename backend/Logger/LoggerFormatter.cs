namespace backend.Logger
{
    internal class LoggerFormatter
    {
        public string ApplyFormat(LogMessage logMessage)
        {
            return logMessage.TraceId == null
                ? $"{logMessage.DateTime:G}: {logMessage.Level.ToString()}: {logMessage.Text}"
                : $"{logMessage.DateTime:G}: {logMessage.Level.ToString()}: {logMessage.TraceId.ToString()} : {logMessage.Text}";
        }
    }
}