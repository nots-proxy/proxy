using System;
using System.Net;
using System.Threading.Tasks;

namespace backend.Models
{
    public interface IRequest
    {
        public Uri RequestUri { get; }
        WebHeaderCollection Headers { get; }

        public Task<IResponse> GetResponse();
    }
}