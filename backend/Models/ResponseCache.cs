using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace backend.Models
{
    public class ResponseCache : Response
    {
       
        private byte[] _value;

        private ResponseCache(HttpWebResponse response) : base(response) { }

        public static async Task<ResponseCache> From(HttpWebResponse response)
        {
            var rc = new ResponseCache(response);
            await rc.AddValue(response.GetResponseStream());
            return rc;
        }

        private async Task AddValue(Stream stream)
        {
            if (stream.CanSeek)
                stream.Position = 0;
            
            var memoryStream = new MemoryStream();
            await stream.CopyToAsync(memoryStream);
            _value = memoryStream.GetBuffer();
        }
        
        public override Stream GetResponseStream() => new MemoryStream(_value);
    }
}