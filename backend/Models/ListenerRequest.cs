using System;
using System.IO;
using System.Net;
using backend.Extensions;

namespace backend.Models
{
    public interface IListenerRequest
    {
        public Uri RequestUri { get; }
        public string RawUrl { get; }
        public Uri Url { get; }
        public string HttpMethod { get; }
        public string UserAgent { get; }
        public long ContentLength64 { get; }
        public Stream InputStream { get; }
        public WebHeaderCollection Headers { get; }
        
        public Guid RequestTraceIdentifier { get; }
    }

    public class ListenerRequest : IListenerRequest
    {
        private readonly HttpListenerRequest _request;
        public WebHeaderCollection Headers { get; }


        public ListenerRequest(HttpListenerRequest request)
        {
            _request = request;
            Headers = request.Headers.ToWebHeaderCollection();
        }

        public Guid RequestTraceIdentifier => _request.RequestTraceIdentifier;

        public Uri RequestUri => _request.Url;

        public Uri Url => _request.Url;

        public string RawUrl => _request.RawUrl;

        public string HttpMethod => _request.HttpMethod;

        public string UserAgent => _request.UserAgent;

        public long ContentLength64 => _request.ContentLength64;

        public Stream InputStream => _request.InputStream;
    }
}