using System;
using System.IO;
using System.Net;

namespace backend.Models
{
    public interface IResponse
    {
        public Uri ResponseUri { get; }
        public CookieCollection Cookies { get; }
        public WebHeaderCollection Headers { get; }
        public Stream GetResponseStream();
        public HttpStatusCode StatusCode { get; }
    }
}