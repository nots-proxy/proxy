using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using backend.Exceptions;
using backend.Logger;
using backend.Models;
using backend.Web;

namespace backend.Caching
{
    public class CacheManager
    {
        private static readonly string[] CacheControlHeaders = {"public", "no-cache", "must-revalidate", "max-age", "immutable"};
        private static readonly string[] NoCacheControlHeaders = {"private", "no-store"};
        
        private readonly CacheStore _cacheStore= new CacheStore();
        // Create a setter for tests.
        private readonly CacheValidator _cacheValidator = new CacheValidator();
        private readonly ClientHeaderValidator _clientHeaderValidator = new ClientHeaderValidator();
        public async Task<IResponse> GetRequest(IListenerRequest request)
        {
            // // Return result from cache
            if (Config.GetInstance().Options.IsCachingEnabled && _cacheStore.ContainsKey(request.RawUrl))
            {
                var result = GetResultFromCache(request.RawUrl);
                if (result != null)
                {
                    if (_cacheValidator.IsValid(result))
                    {
                        ProxyLogger.Log(Level.Info, "Returning result from cache", request.RequestTraceIdentifier);
                        return result;
                    }
                    ProxyLogger.Log(Level.Info, "Cache outdated", request.RequestTraceIdentifier);
                    _cacheStore.PossibleRemoval(request.RawUrl);
                }
            }

            if (_clientHeaderValidator.AcceptOnlyCached(request.Headers))
            {
                throw new WebResponseException(HttpStatusCode.GatewayTimeout, "No result cached", request.RequestTraceIdentifier);
            }
            
            // Get request from the web
            var webResponse = await GetResultFromWeb(request);

            if (IsCacheable(request.HttpMethod, webResponse.Headers) && Config.GetInstance().Options.IsCachingEnabled)
            {
                ProxyLogger.Log(Level.Info, "Adding request to cache", request.RequestTraceIdentifier);
                _cacheStore.Add(request.RawUrl, await webResponse.ToResponseCache());
                return _cacheStore.GetCache(request.RawUrl);
            }

            return webResponse;
        }

        private IResponse GetResultFromCache(string key) => _cacheStore.GetCache(key);
        private async Task<Response> GetResultFromWeb(IListenerRequest request)
        {
            var wManager = new WebManager();
            var webRequest = await wManager.BuildWebRequest(request);
            var webResponse = (HttpWebResponse) await webRequest.GetResponseAsync();
            return new Response(webResponse);
        }

        public bool IsCacheable(string requestMethod, WebHeaderCollection responseHeaders)
        {
            if (requestMethod != "GET" && requestMethod != "HEAD")
                return false;

            var teHeader = responseHeaders.Get("Transfer-Encoding");
            if (teHeader != null)
            {
                if (teHeader.Contains("chunked")) return false;
            }

            var clHeader = responseHeaders.Get("Content-Length");
            if (clHeader != null && int.TryParse(clHeader, out var length) )
            {
                if (length >= 500000) return false;
            }

            if (responseHeaders.Get("Cache-Control") != null)
            {
                var ccHeaders = responseHeaders.Get("Cache-Control");
                if (NoCacheControlHeaders.Any(ccHeaders.Contains))
                    return false;
                if (CacheControlHeaders.Any(ccHeaders.Contains))
                    return true;
            }

            if (responseHeaders.Get("Expires") != null)
                return DateTime.TryParse(responseHeaders.Get("Expires"), out _);

            return responseHeaders.Get("Pragma") != null && responseHeaders.Get("Pragma").Contains("no-cache");
        }
    }
}