using System;
using System.Linq;
using backend.Models;

namespace backend.Caching.Strategy
{
    public class ExpireCategory
    {
        public string Value { get; }
        private ExpireCategory(string value)
        {
            Value = value; 
        }
        
        public static ExpireCategory SMaxAge => new ExpireCategory("s-maxage");
        public static ExpireCategory MaxAge => new ExpireCategory("max-age");
        public static ExpireCategory MaxStale => new ExpireCategory("max-stale");
    }
    
    public class ExpiresStrategy : ICachingStrategy
    {
        private readonly DateTime _expiresDate;

        public ExpiresStrategy(IResponse response)
        {
            var dHeader = response.Headers.Get("Date");
            var ccHeader = response.Headers.Get("Cache-Control");

            _expiresDate = ccHeader switch
            {
                var header when header.Contains(ExpireCategory.SMaxAge.Value) => 
                    GetExpireDate(dHeader, ccHeader, ExpireCategory.SMaxAge),
                var header when header.Contains(ExpireCategory.MaxAge.Value) =>
                    GetExpireDate(dHeader, ccHeader, ExpireCategory.MaxAge),
                _ => throw new Exception("No Expire strategy found")
            };

            if (ccHeader.Contains("max-stale"))
            {
                var seconds = GetStaleSeconds(ccHeader);
                _expiresDate = _expiresDate.AddSeconds(seconds);
            }
        }
        
        private DateTime GetExpireDate(string date, string ccHeader, ExpireCategory category)
        {
            var responseDate = DateTime.Parse(date);
            var seconds = GetSecondsFromHeader(ccHeader, category);
            return responseDate.AddSeconds(seconds);
        }

        private int GetStaleSeconds(string ccHeader)
        {
            return GetSecondsFromHeader(ccHeader, ExpireCategory.MaxStale);
        }

        private int GetSecondsFromHeader(string ccHeader, ExpireCategory category)
        {
            var maxAgeString = ccHeader
                .Split(",")
                .First(s => s.Contains(category.Value))
                .Split("=")
                .Last();
            return int.Parse(maxAgeString);
        }

        public bool IsValid()
        {
            return _expiresDate >= DateTime.Now;
        }
    }
}