namespace backend.Caching.Strategy
{
    public interface ICachingStrategy
    {
        public bool IsValid();
    }
}