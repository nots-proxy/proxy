using Xunit;

namespace backend.Caching.Strategy
{
    public class ImmutableStrategyTest
    {
        [Fact]
        public void ImmutableTest()
        {
            // Arrange
            var strategy = new ImmutableStrategy();
            
            // Act
            var isValid = strategy.IsValid();
            
            // Assert
            Assert.True(isValid);
        }
    }
}