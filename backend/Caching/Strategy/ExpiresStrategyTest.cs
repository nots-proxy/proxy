using System;
using System.Globalization;
using backend.Models;
using Moq;
using Xunit;

namespace backend.Caching.Strategy
{
    public class ExpiresStrategyTest
    {
        [Fact]
        public void IsExpiredTest()
        {
            // Arrange
            var date = DateTime.Now.AddDays(-1).ToString("r");
            const int maxAgeSeconds = 600;
            
            var request = new Mock<IResponse>();
            request.Setup(r => r.Headers.Get("Date")).Returns(date);
            request.Setup(r => r.Headers.Get("Cache-Control")).Returns($"public, max-age={maxAgeSeconds}");
            
            var strategy = new ExpiresStrategy(request.Object);
            
            // Act
            var isValid = strategy.IsValid();

            // Assert
            Assert.False(isValid);
        }
        
        [Fact]
        public void IsNotExpiredTest()
        {
            // Arrange
            var date = DateTime.Now.AddDays(1).ToString("r");
            const int maxAgeSeconds = 600;
            
            var request = new Mock<IResponse>();
            request.Setup(r => r.Headers.Get("Date")).Returns(date);
            request.Setup(r => r.Headers.Get("Cache-Control")).Returns($"public, max-age={maxAgeSeconds}");
            
            var strategy = new ExpiresStrategy(request.Object);
            
            // Act
            var isValid = strategy.IsValid();

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void SMaxAgeTest()
        {
            // Arrange
            var date = DateTime.Now.AddDays(1).ToString("r");
            const int maxAgeSeconds = 600;
            
            var request = new Mock<IResponse>();
            request.Setup(r => r.Headers.Get("Date")).Returns(date);
            request.Setup(r => r.Headers.Get("Cache-Control")).Returns($"public, s-maxage={maxAgeSeconds}");
            
            var strategy = new ExpiresStrategy(request.Object);
            
            // Act
            var isValid = strategy.IsValid();

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void SMaxAgeBeforeMaxAgeTest()
        {
            // Arrange
            const int sMaxAgeSeconds = 2000;
            var date = DateTime.Now.ToString(CultureInfo.InvariantCulture);
            const int maxAgeSeconds = 600;
            
            var request = new Mock<IResponse>();
            request.Setup(r => r.Headers.Get("Date")).Returns(date);
            request.Setup(r => r.Headers.Get("Cache-Control")).Returns($"public, max-age={maxAgeSeconds} s-maxage={sMaxAgeSeconds}");
            
            var strategy = new ExpiresStrategy(request.Object);
            
            // Act
            var isValid = strategy.IsValid();

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void NoAvailableStrategy()
        {
            // Arrange
            var date = DateTime.Now.ToString("r");
            var request = new Mock<IResponse>();
            request.Setup(r => r.Headers.Get("Date")).Returns(date);
            request.Setup(r => r.Headers.Get("Cache-Control")).Returns($"public");

            // Assert
            Assert.Throws<Exception>(() => new ExpiresStrategy(request.Object));
        }
    }
}