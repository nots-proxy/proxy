using System.Net;
using System.Threading.Tasks;
using backend.Models;
using backend.Web;
using Moq;
using Xunit;

namespace backend.Caching.Strategy
{
    public class RevalidateStrategyTest
    {
        [Fact]
        public void RevalidateNotModified()
        {
            // Arrange
            var request = new Mock<IRequest>();
            var response = new Mock<IResponse>();
            response.Setup(r => r.StatusCode).Returns(HttpStatusCode.NotModified);
            request.Setup(r => r.GetResponse()).Returns(Task.FromResult(response.Object));
            
            var wManager = new Mock<IWebManager>();
            var webResponse = new Mock<IResponse>();
            wManager.Setup(m => m.BuildValidateRequest(webResponse.Object)).Returns(request.Object);
            var strategy = new RevalidateStrategy(webResponse.Object, wManager.Object);

            // Act
            var res = strategy.IsValid();
            
            //Assert
            Assert.True(res);
        }
        
        [Fact]
        public void RevalidateIsModified()
        {
            // Arrange
            var request = new Mock<IRequest>();
            var response = new Mock<IResponse>();
            response.Setup(r => r.StatusCode).Returns(HttpStatusCode.OK);
            request.Setup(r => r.GetResponse()).Returns(Task.FromResult(response.Object));
            
            var wManager = new Mock<IWebManager>();
            var webResponse = new Mock<IResponse>();
            wManager.Setup(m => m.BuildValidateRequest(webResponse.Object)).Returns(request.Object);
            
            var strategy = new RevalidateStrategy(webResponse.Object, wManager.Object);

            // Act
            var res = strategy.IsValid();
            
            //Assert
            Assert.False(res);
        }
    }
}