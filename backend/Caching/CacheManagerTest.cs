using System;
using System.Net;
using Xunit;

namespace backend.Caching
{
    public class CacheManagerTest
    {
        [Theory]
        [InlineData("GET", true)]
        [InlineData("HEAD", true)]
        [InlineData("POST", false)]
        [InlineData("PUT", false)]
        public void RequestTypeCacheable(string httpMethod, bool isCacheable)
        {
            // Arrange
            var cacheManager = new CacheManager();
            var headers = new WebHeaderCollection()
            {
                {"Cache-Control", "public, max-age=200, immutable"}
            };

            // Act
            var res = cacheManager.IsCacheable(httpMethod, headers);
            
            // Assert
            Assert.Equal(isCacheable, res);
        }
        
        [Theory]
        [InlineData("public, max-age=200, immutable", true)]
        [InlineData("private, max-age=200, immutable", false)]
        [InlineData("no-store, max-age=200, immutable", false)]
        [InlineData("no-cache, max-age=200, immutable", true)]
        [InlineData("public, max-age=0, immutable", true)]
        [InlineData("no-store, max-age=0", false)]
        public void RequestCacheControlCacheable(string header, bool isCacheable)
        {
            // Arrange
            var cacheManager = new CacheManager();
            var headers = new WebHeaderCollection
            {
                {"Cache-Control", header}
            };

            // Act
            var res = cacheManager.IsCacheable("GET", headers);
            
            // Assert
            Assert.Equal(isCacheable, res);
        }
        
        [Fact]
        public void RequestPragmaCacheable()
        {
            // Arrange
            var cacheManager = new CacheManager();
            var headers = new WebHeaderCollection
            {
                {"Pragma", "no-cache"}
            };

            // Act
            var res = cacheManager.IsCacheable("GET", headers);
            
            // Assert
            Assert.True(res);
        }

        public static readonly object[][] ExpiresData =
        {
            new object[] {"kjasdnfkjankdna", false},
            new object[] {DateTime.Now.AddDays(1).ToString("r"), true},
        };
        
        [Theory, MemberData(nameof(ExpiresData))]
        public void RequestExpiresCacheable(string date, bool isCacheable)
        {
            // Arrange
            var cacheManager = new CacheManager();
            var headers = new WebHeaderCollection
            {
                {"Expires", date}
            };

            // Act
            var res = cacheManager.IsCacheable("GET", headers);
            
            // Assert
            Assert.Equal(isCacheable, res);
        }
    }
    
}