using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace backend
{
    public class ProxyTest
    {
        [Theory (Skip = "Proxy should be running, run manually")]
        [InlineData(
            "http://httpvshttps.com",
            "http://nots-proxy.gitlab.io/http1_1_website/microscape.html",
            "http://cbfdhnmrsvktlwzx.neverssl.com/online",
            "http://captive.apple.com/",
            "http://www.reddit.com/",
            "http://www.google.com/",
            "http://google.com/generate_204",
            "http://www.w3.org/Protocols/HTTP/Performance/microscape/",
            "http://www.w3.org/Library/Overview.html"
            )]
        public void ProxyLoadTest(params string[] urls)
        {
            const int numberOfClients = 40;
            var webProxy = new WebProxy("http://localhost:80", true);

            var result = Parallel.For(0, numberOfClients, async (_) =>
            {
                for (var i = 0; i < 20; i++)
                {
                    var random = new Random().Next(urls.Length);
                    var client = WebRequest.Create(urls[random]);
                    client.Proxy = webProxy;
                    try
                    {
                        var res = (HttpWebResponse) await client.GetResponseAsync();
                        Assert.True(res.StatusCode == HttpStatusCode.OK);
                        res.Close();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            });

            while (!result.IsCompleted) {}
        }
    }
}