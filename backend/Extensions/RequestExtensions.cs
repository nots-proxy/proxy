using System.Collections.Specialized;
using System.Linq;
using System.Net;

namespace backend.Extensions
{
    public static class RequestExtension
    {
        public static void AddHeaders(this HttpWebRequest request, NameValueCollection headers)
        {
            var headerWithValues =
                request.Headers.AllKeys.SelectMany(request.Headers.GetValues, (k, v) => new {key = k, value = v});

            foreach (var header in headerWithValues)
            {
                request.Headers.Add(header.key, header.value);
            }
        }
    }

    // public static class StreamExtension
    // {
    //     public static Task CopyToMultipleAsync(this Stream sourceStream, List<Stream> streams)
    //     {
    //         var bufferSize = GetBufferSize(sourceStream);
    //         return CopyToMultipleAsync(sourceStream, bufferSize, streams);
    //     }
    //
    //     private static async Task CopyToMultipleAsync(this Stream sourceStream, int bufferSize,
    //         IReadOnlyCollection<Stream> streams)
    //     {
    //         var buffer = ArrayPool<byte>.Shared.Rent(bufferSize);
    //         while (true)
    //         {
    //             var bytesRead = await sourceStream.ReadAsync(new Memory<byte>(buffer)).ConfigureAwait(false);
    //             if (bytesRead == 0) break;
    //
    //             Task.WaitAll(streams.Select(stream => Task.Run(() =>
    //                     stream.WriteAsync(new ReadOnlyMemory<byte>(buffer, 0, bytesRead)).ConfigureAwait(false)))
    //                 .Cast<Task>().ToArray());
    //         }
    //     }
    //
    //     private static int GetBufferSize(Stream stream)
    //     {
    //         var bufferSize = 81920;
    //
    //         if (stream.CanSeek)
    //         {
    //             var length = stream.Length;
    //             var position = stream.Position;
    //
    //             if (length <= position)
    //             {
    //                 bufferSize = 1;
    //             }
    //             else
    //             {
    //                 var remaining = length - position;
    //                 if (remaining > 0)
    //                 {
    //                     bufferSize = (int) Math.Min(bufferSize, remaining);
    //                 }
    //             }
    //         }
    //
    //         return bufferSize;
    //     }
    // }
}
