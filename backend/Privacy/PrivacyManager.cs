using System.Net;
using backend.Logger;
using backend.Models;

namespace backend.Privacy
{
    public class PrivacyManager
    {
        public void RemoveClientHeaders(IListenerRequest request)
        {
            ProxyLogger.Log(Level.Info, "Removing privacy headers", request.RequestTraceIdentifier);
            //   "MOZILLA/5.0 (WINDOWS NT 6.1; WOW64) APPLEWEBKIT/537.1 (KHTML, LIKE GECKO) CHROME/21.0.1180.75 SAFARI/537.1";
            request.Headers.Remove(HttpRequestHeader.UserAgent);
            request.Headers.Remove(HttpRequestHeader.AcceptLanguage);
            request.Headers.Remove(HttpRequestHeader.Referer);
            request.Headers.Remove("Forwarded");
            request.Headers.Remove("DNT");
            request.Headers.Remove("Origin");
        }
    }
}