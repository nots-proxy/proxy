using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using backend.Exceptions;
using backend.Extensions;
using backend.Logger;
using backend.Models;

namespace backend
{
    public class ResponseHandler
    {
        public async void HandleResponse(HttpListenerResponse response, IResponse externalRes,
            IListenerRequest listenerRequest)
        {
            response.Cookies = externalRes.Cookies;
            response.Headers = externalRes.Headers;

            try
            {
                var exResponseStream = externalRes.GetResponseStream();

                if (exResponseStream.CanSeek)
                {
                    exResponseStream.Position = 0;
                }

                await exResponseStream.CopyToAsync(response.OutputStream);

                response.Close();
            }
            catch (Exception e)
            {
                ProxyLogger.Log(e, listenerRequest.RequestTraceIdentifier);
            }
        }

        public async Task HandleCustomResponseError(HttpListenerResponse response, WebResponseException exception,
            IListenerRequest listenerRequest)
        {
            try
            {
                ProxyLogger.Log(Level.Info, exception.Message, exception.TraceId);
                response.StatusCode = (int) exception.StatusCode;
                await exception.Message.ToStream().CopyToAsync(response.OutputStream);
                response.Close();
            }
            catch (Exception e)
            {
                ProxyLogger.Log(e, listenerRequest.RequestTraceIdentifier);
            }
        }

        public async Task HandleResponseError(HttpListenerResponse response, WebException webException,
            IListenerRequest listenerRequest)
        {
            var externalResponse = (HttpWebResponse) webException.Response;

            if (externalResponse == null)
            {
                response.StatusCode = 500;
                var message = Encoding.UTF8.GetBytes(webException.Message);
                response.OutputStream.Write(message);
                response.Close();
                return;
            }

            try
            {
                response.StatusCode = (int) externalResponse.StatusCode;
                response.Headers = externalResponse.Headers;
                response.Cookies = externalResponse.Cookies;

                var externalStream = externalResponse.GetResponseStream();
                if (externalStream != null) await externalStream.CopyToAsync(response.OutputStream);

                response.Close();
            }
            catch (Exception e)
            {
                response.Close();
                ProxyLogger.Log(e, listenerRequest.RequestTraceIdentifier);
            }
        }
    }
}