using System.Net;
using System.Text;
using backend.Models;

namespace backend.Auth
{
    public class AuthManager
    {
        public bool IsAuthorized(HttpListenerBasicIdentity identity)
        {
            if (!Config.GetInstance().Options.IsBasicAuthEnabled) return true;
            if (identity == null) return true;
            var credentials = Config.GetInstance().Options.AuthCredentials;
            return identity.Name == credentials.Username && identity.Password == credentials.Password;
        }

        public void HandleUnauthorized(HttpListenerResponse response)
        {
            response.StatusCode = 401;
            response.AddHeader("WWW-authenticate", "Basic Realm=\"Custom proxy\"");
            var message = new UTF8Encoding().GetBytes("Access denied");
            response.ContentLength64 = message.Length;
            response.OutputStream.Write(message);
            response.Close();
        }

        public void RemoveAuthHeaders(IListenerRequest request)
        {
            request.Headers.Remove("Authorization");
            request.Headers.Remove("Upgrade-Insecure-Requests");
        }
    }
}