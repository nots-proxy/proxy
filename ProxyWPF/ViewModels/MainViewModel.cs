﻿using ProxyWPF.Common;
using Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace ProxyWPF.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        public MainViewModel()
        {
            SaveHosts = new RelayCommand(ExecuteSaveHosts, CanExecuteSaveHosts);
            Hosts = string.Join(Environment.NewLine, App.Config.BlockList);
            App.Config.PropertyChanged += DidHostsChange;
        }

        private bool _isRunning;
        public bool IsRunning
        {
            get => _isRunning;
            set
            {
                if (value)
                    App.Proxy.Start();
                else
                    App.Proxy.Stop();

                Set(ref _isRunning, value);
            }
        }

        public int Port
        {
            get => App.Config.Port;
            set
            {
                App.Config.Port = value;
                OnPropertyChanged();
            }
        }
        public bool IsLogging
        {
            get => App.Config.IsLoggingEnabled;
            set
            {
                App.Config.IsLoggingEnabled = value;
                OnPropertyChanged();
            }
        }

        public bool IsCaching
        {
            get => App.Config.IsCachingEnabled;
            set
            {
                App.Config.IsCachingEnabled = value;
                OnPropertyChanged();
            }
        }

        public bool IsBasicAuthOn
        {
            get => App.Config.IsBasicAuthEnabled;
            set
            {
                bool credentialsSet = string.IsNullOrEmpty(App.Config.AuthCredentials.Password)
                                      || string.IsNullOrEmpty(App.Config.AuthCredentials.Username);

                App.Config.IsBasicAuthEnabled = !credentialsSet && value;
                OnPropertyChanged();
            }
        }


        public string Username
        {
            get => App.Config.AuthCredentials.Username;
            set
            {
                App.Config.AuthCredentials.Username = value;
                OnPropertyChanged("");
            }
        }

        private string _usernameStatusText;
        public string UsernameStatusText
        {
            get => _usernameStatusText;
            set => Set(ref _usernameStatusText, value);
        }

        private string _passwordStatusText;
        public string PasswordStatusText
        {
            get => _passwordStatusText;
            private set => Set(ref _passwordStatusText, value);
        }

        public void PasswordChanged(object sender, RoutedEventArgs _)
        {
            PasswordBox passwordBox = (PasswordBox)sender;

            if (PasswordIsValid(passwordBox.Password))
            {
                App.Config.AuthCredentials.Password = passwordBox.Password;
                PasswordStatusText = string.Empty;
            }
            else
            {
                PasswordStatusText = "Password is not valid";
                App.Config.AuthCredentials.Password = null;
            }
        }

        private bool PasswordIsValid(string password) => password != "Password";

        private string _saveButtonContent = "Save";
        public string SaveButtonContent
        {
            get => _saveButtonContent;
            set => Set(ref _saveButtonContent, value);
        }

        public RelayCommand SaveHosts;

        private string _hosts;
        public string Hosts
        {
            get => _hosts;
            set
            {
                Set(ref _hosts, value);
                SaveHosts.RaiseCanExecuteChanged();
            }
        }

        private void DidHostsChange(object sender, PropertyChangedEventArgs e)
        {
            var config = (ProxyConfig)sender;
            var currentHosts = Hosts.Split(
                "\r",
                StringSplitOptions.RemoveEmptyEntries).ToList();

            if (!Equals(currentHosts, config.BlockList))
            {
                Hosts = string.Join(Environment.NewLine, config.BlockList);
            }
        }

        private void ExecuteSaveHosts()
        {
            // BUG: UWP textbox does not use the environment newline. Only \r.
            var hosts = Hosts.Split(
                "\r",
                StringSplitOptions.RemoveEmptyEntries).ToList();

            App.Config.BlockList = hosts;
        }

        private bool CanExecuteSaveHosts() => !string.IsNullOrEmpty(Hosts);

        public bool IsContentBlocking
        {
            get => App.Config.IsContentBlockingEnabled;
            set
            {
                App.Config.IsContentBlockingEnabled = value;
                OnPropertyChanged();
            }
        }

        public bool IsProtectingPrivacy
        {
            get => App.Config.IsPrivacyEnabled;
            set
            {
                App.Config.IsPrivacyEnabled = value;
                OnPropertyChanged();
            }
        }

        public bool IsBlockingHosts
        {
            get => App.Config.IsUrlBlockingEnabled;
            set
            {
                App.Config.IsUrlBlockingEnabled = value;
                OnPropertyChanged();
            }
        }

    }
}
