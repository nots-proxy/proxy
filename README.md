# Proxy

Voor dit project is een HTTP Proxy geschreven in C#. De proxy maakt gebruik van de c# `HttpListener` en ondersteund Caching, Basic Auth, logging en Content/Host blocking. De code is opgedeeld in twee projecten. Voor het draaien van de applicatie moet het `Testing` project gestart worden. De Proxy code zelf staat in het `Proxy` project. Hieronder staat een afbeelding van de proxy UI uit het `Testing` project.

![Proxy](./resources/Proxy.png)

(Figuur 1. Screenshot proxy UI)

- [Proxy](#proxy)
  - [1. Architecture](#1-architecture)
  - [1.1. Klassendiagram](#11-klassendiagram)
  - [2. TCP/IP](#2-tcpip)
    - [2.1. Alternatieven & Adviezen](#21-alternatieven--adviezen)
  - [3. HTTP](#3-http)
    - [3.1. HTTP bericht opbouw](#31-http-bericht-opbouw)
      - [3.1.1. Start-line](#311-start-line)
      - [3.1.2. Headers](#312-headers)
        - [3.1.2.1. General-header](#3121-general-header)
        - [3.1.2.2. Request-header](#3122-request-header)
        - [3.1.2.3. Response-header](#3123-response-header)
        - [3.1.2.4. Entity-header](#3124-entity-header)
      - [3.1.3. Body](#313-body)
    - [3.2. Hoe wordt de content in een bericht verpakt? (teken een diagram en licht de onderdelen toe)](#32-hoe-wordt-de-content-in-een-bericht-verpakt-teken-een-diagram-en-licht-de-onderdelen-toe)
    - [3.3. Streaming Content](#33-streaming-content)
    - [3.4. Overeenkomsten request en response](#34-overeenkomsten-request-en-response)
  - [4. Reflectie](#4-reflectie)
  - [5. Test cases](#5-test-cases)
    - [5.1. Caching](#51-caching)
      - [5.1.1. Caching](#511-caching)
      - [5.1.2. Cache Timeout](#512-cache-timeout)
    - [5.2. Filter ongewenste content](#52-filter-ongewenste-content)
    - [5.4. Identiteit](#54-identiteit)
      - [5.4.1. Protect Privacy](#541-protect-privacy)
      - [5.4.2. Basic Auth](#542-basic-auth)
    - [5.5. Buffersize](#55-buffersize)
    - [5.6 Geschikt voor tekst en binaire data](#56-geschikt-voor-tekst-en-binaire-data)

## 1. Architecture

Om de flow door de applicatie duidelijk te maken staat hieronder een flowchart. In de eerste stap wordt er gekeken of er authenticatie nodig is. Daarna gaat het request door verschillende filters heen.

![Proxy flow chart](./resources/FlowChart.png)

(Figuur 2. Proxy flowchart)

Er zijn een aantal opties die aan en uitgeschakeld kunnen worden. Als de privacy optie is ingeschakeld worden `User-Agent`, `Accept-Language`, `Referer`, `Forwarded`, `DNT` en `Origin` header verwijderd uit het request. Deze headers worden gebruikt als finger-print om iemands gedrag te volgens over het internet.

Na het privacy filter gaat het request door de Content filter. Zoals beschreven in de opdracht om advertenties te blokkeren. In de huidige implementatie worden afbeeldingen vervangen door een afbeelding van 1x1 pixel. Door deze pixel terug te sturen is er meer kans dat de website blijft werken zoals bedoelt en wordt de layout zo min mogelijk aangetast.

Als laatste is er een url filter. Deze kijkt of er bepaalde hosts ongewenst zijn. Standaard blokeert deze applicatie `platform.twitter.com` en `gstatic.com`.

Als het request door alle filters is gekomen wordt het request opgehaald. Dit kan gebeuren door deze uit de cache te halen of een request te maken naar het internet. Eerst wordt er gekeken of het request al beschikbaar is in de cache en of deze nog valide is. Mocht dit zo zijn dan wordt de cache teruggeven. Is de cache niet beschikbaar of invalide, dan wordt er eerst gekeken of de client een `only-if-cached` header heeft meegestuurd. Mocht dit zo zijn dan wordt er een status `504 Gateway Timeout` teruggestuurd. Als de client deze header niet meestuurt wordt er een request gemaakt naar het internet. Als het request terugkomt wordt er gekeken of deze opgeslagen kan worden in de cache. Als laatste wordt het externe request teruggestuurd naar de client.

## 1.1. Klassendiagram

Figuur 3 tot en met 5 zijn de klassendiagram van de proxy. Figuur 3 heeft de Proxy class waar je de proxy mee start zowel als de configuratie, logging en filters. Figuur 4 zijn alle klassen die te maken hebben met caching en Figuur 5 zijn de models.

![Proxy class diagram 1](./resources/ProxyClassDiagram_1.png)

(Figuur 3. Configuratie, Log, Authorizatie, Filter klassendiagram)

![Proxy class diagram 2](./resources/ProxyClassDiagram_2.png)

(Figuur 4. Caching klassendiagram)

![Proxy class diagram 3](./resources/ProxyClassDiagram_3.png)

(Figuur 5. Models klassendiagram)

## 2. TCP/IP

`TCP/IP` staat voor Transmisson Control Protocol over Internet Protocol. `IP` is een netwerkprotocol waarmee computers in een netwerk kunnen communiceren. `IP` heeft als doel om pakketten van het ene `IP` adres naar het andere adres te sturen. Het `TCP` protocol zorgt ervoor dat de data dat verstuurd moet worden opgeknipt wordt in packets en verstuurd naar de ontvanger. Bij de ontvanger kan `TCP` daarna de packets weer in je juiste volgorde achter elkaar zetten zonder data verlies. Mochten er packets verloren zijn of beschadigd zijn geraakt op het internet kan `TCP` deze herstellen door data opnieuw op de vragen. Hierdoor neemt wel de netwerkvertraging toe.([RFC1122](https://tools.ietf.org/html/rfc1122#section-1.1.3))

### 2.1. Alternatieven & Adviezen

Een alternatief voor `TCP/IP` is `UDP/IP`. `UDP` staat voor User Datagram Protocol en is een protocol dat in tegestelling `TCP` geen rekening houdt met volgorde, verdwenen of corrupte packets. Hierdoor is het een veel lichter en sneller protocol. UDP wordt vaak gebruikt voor tijd gebonden applicaties zoals gaming of video bellen. Het heeft geen zin om data opnieuw op te vragen als de video of game al door gaat.([RFC1122](https://tools.ietf.org/html/rfc1122#section-1.1.3))

Een wat minder gangbaar protocol is `ICMP`. `ICMP` is bedoeld om berichten voor de `TCP` software zelf te versturen en ontvangen. Het is daardoor niet makkelijk om berichten van hiermee te versturen in dit scenario.([RFC792](https://tools.ietf.org/html/rfc792))

Een ander alternatief is AppleTalk. AppleTalk is een voorloper van `TCP/IP`. Dit protocol is door Apple ontworpen om over het netwerk met printers, routers en bestandsservers te communiceren.([Inside AppleTalk, Second edition](https://vintageapple.org/macbooks/pdf/Inside_AppleTalk_Second_Edition_1990.pdf))

Het advies zou zijn om voor tijdgebonden content `UDP/IP` te gebruiken en bij data dat altijd compleet moet zijn `TCP/IP`.

## 3. HTTP

HTTP staat voor HyperText Transfer Protocol. Het is de fundatie van de communicatie van het World Wide Web.

### 3.1. HTTP bericht opbouw

Elk http bericht maakt gebruik van een algemeen formaat beschreven in [RFC 822](https://tools.ietf.org/html/rfc822). In [RFC2616 4.1 Message Types](https://www.ietf.org/rfc/rfc2616.txt) staat dat een http bericht bestaat uit een start-line, headers, empty line en een eventuele body. Een voorbeeld van een HTTP bericht is hieronder weergegeven:

![HTTP-request en HTTP response voorbeeld](./resources/httpmsgstructure2.png)

(Figuur 6. Request Response voorbeeld HTTP bericht [MDN Web Docs](https://developer.mozilla.org/en-US/docs/Web/HTTP/Messages))

|Http opbouw|Uitleg|
|---|---|
|start-line|De start-line is het eerste regel in een HTTP bericht. Voor een request wordt deze regel Request-Line genoemd en bij een response Status-Line. |
|headers|headers worden gebruikt om informatie over het bericht, body, afzender of server mee te sturen|
|CRLF (lege regel)|De lege regel geeft de scheiding aan tussen headers en body|
|Body (optioneel)|De body is de payload die meegestuurd kan worden bij een http request of response|

#### 3.1.1. Start-line

De start-line is de eerste regel van een http bericht. Afhankelijk van of het bericht een request of response is, is deze data iets anders. Bij een request staat eerst de [methode](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods), daarna de [URI](https://tools.ietf.org/html/rfc3986) en als laatste de [HTTP versie](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Evolution_of_HTTP). Deze regel wordt de `Request-Line` genoemd. Bij een response bericht wordt dezelfde regel de `Status-Line` genoemd. In deze regel staat dan de HTTP versie, Status en een textuele uitleg van de status. Hieronder een voorbeeld van Request- en Status-Line.

Request-Line:

|Line|Methode|URI|HTTP Versie|
|---|---|---|---|
|`GET / HTTP/2`|GET|/|HTTP/2|
|`POST /posts HTTP/1.1`|POST|/posts|HTTP/1.1|
|`HEAD /posts/1 HTTP/1.0`|HEAD|/posts/1|HTTP/1.0|

Status-Line:

|Line|HTTP Versie|Status code|Tekst|
|---|---|---|---|
|`HTTP/1.1 200 OK`|HTTP/1.1|200|OK|
|`HTTP/1.0 500 Internal Server Error`|HTTP/1.0|500|Internal Server Error|
|`HTTP/2 400 Bad Request`|HTTP/2|400|Bad Request|

#### 3.1.2. Headers

Headers zijn opgedeelte uit 4 header groepen. General, Request, Response en Entity. Deze headers staan meestal in de volgende volgorde van Request/Response headers, General headers en dan de Entity headers.

|Headers|
|---|
|Request/Response headers|
|General headers|
|Entity headers|

##### 3.1.2.1. General-header

De headers in deze groep kunnen worden gebruikt voor zowel requests als reponses. Deze headers zeggen alleen iets over het http bericht zelf en niet over de body die mee gestuurd wordt. Denk hierbij aan: `Cache-Control`, `Date` of `Transfer-Encoding`.

##### 3.1.2.2. Request-header

Deze headers zijn specifiek voor een http request. Deze headers geven de mogelijkheid om meer informatie te geven over het bericht of over de afzender van het bericht. Een voorbeeld hiervan is de `User-Agent` header.

##### 3.1.2.3. Response-header

Deze headers geven de mogelijkheid om meer informatie over de response dat niet in de [start-line](####Start-line) past. Deze headers geven informatie over de server en resource. Denk hierbij aan de `Server` en `WWW-Authenticate` header.

##### 3.1.2.4. Entity-header

De entity headers geven metadata over de body die wordt meegestuurd in het bericht. Als een body wordt meegestuurd zijn een aantal hiervan verplicht(`Content-type` en `Content-Length`) en de rest is optioneel. `Content-Length` mag vervallen als `Transfer-Encoding: chucked` is.

#### 3.1.3. Body

In de body staat de data die de afzender wilt meesturen. Deze data wordt volgens de RFC spec Entity genoemd. Dit kunnen afbeelding, videos, HTML of andere bestanden zijn. De data kan een bestand zijn met een vaste lengte of een bestand met onbekende lengte. Ook is de mogelijkheid om de data de versturen over meerdere entities. Echter wordt dit niet vaak gedaan.

Volgens de [RFC2616](https://www.ietf.org/rfc/rfc2616.txt) spec zou er geen body meegestuurd mogen worden bij een `GET` request. Echter is dit aangepast in [RFC7231](https://tools.ietf.org/html/rfc7231#page-24), hierin staat dat de methode van het request geen invloed heeft of er een body wordt meegestuurd. Maar door een payload mee te sturen met een `GET` request kunnen er bepaalde implementaties omvallen. Een website waar het meesturen van een payload met `GET` gebeurd is [iSAS](https://isas.han.nl)

### 3.2. Hoe wordt de content in een bericht verpakt? (teken een diagram en licht de onderdelen toe)

De content of entity can een bericht staat aan het einde van een HTTP bericht en de lengte hiervan wordt bepaald door de `Content-Length` of the 0 byte bij een `Transfer-Encoding: chucked`. Het voormaat van deze entity kan beschreven zijn in de `Transfer-Encoding` of `Content-Type`. De `Content-Type` specificeerd het bestands type en formaat. Zo'n type wordt ook wel een `MIME type` genoemd. Een aantal zijn veel gebruikt binnen web-development waaronder `plain/text` en `application/json`. De `Transfer-Encoding` duidt aan hoe de entity verpakt is tussen twee computers. Bovenstaand is al geschroken over `chuncked`. Maar er kan ook gekozen worden compressies, zoals `gzip`, `deflate`, of `br`(brotli).

### 3.3. Streaming Content

Bij grote HTTP berichten kan ervoor gekozen worden om deze content te streamen. Hierdoor wordt er steeds een klein beetje data verstuurd. Dit kan door middel van de `Transfer-Encoding: chunked` te gebruiken. Door deze `Transfer-Encoding` hoeft er ook geen `Content-Length` meegestuurd te worden. Bij een normaal bericht kan er dus gekeken worden naar `Content-Length`, maar bij `chunked` niet. Het einde van de body wordt herkend door het sturen van een `chunk` met een lengte van 0. Bovenstaand werkt echter niet meer vanaf `HTTP/2`, omdat daar [DATA frames](https://tools.ietf.org/html/rfc7540) worden gebruikt.

### 3.4. Overeenkomsten request en response

Zoals beschreven in [HTTP bericht opbouw](###31-http-bericht-opbouw) maken beide gebruik van de RFC 822. Hierdoor ziet de structuur er hetzelfde uit. Daarnaast worden voor beide de [General-headers](#####3121-General-header) en [Entity-headers](#####3124-Entity-header) gebruikt. Als laatste is de Entity of body hetzelfde voor zowel een request als response.

## 4. Reflectie

Kwaliteit van een product wordt vooral gekenmerkt door de onderhoudbaarheid, uitbreidbaarheid en performance. Voor onderhoudbaarheid is gekozen voor een package by feature. Hierdoor is in één oogopslag te zien waar welke functionaliteit zich bevindt. Binnen de functionaliteiten Caching en Filtering is ervoor gekozen om een Strategy pattern te gebruiken, dit voor onderhoudbaarheid en uitbreidbaarheid. Een voorbeeld van uitbreidbaarheid was bij de Caching strategie. De ForcedTimeout strategie was niet geïmplementeerd, maar door het strategy pattern was dit zeer eenvoudig. Hier een voorbeeld van de code, Alle strategien zijn conform de ICachingStrategy.

```csharp
// ICachingStrategy.cs
public interface ICachingStrategy
{
    bool IsValid();
}

// CacheValidator.cs
public bool IsValid(IResponse response, ProxyConfig config)
{
    var headers = response.Headers;

    if (config.IsForcedTimeout)
    {
        return new ForcedStrategy(response, config.ForcedTimeout).IsValid();
    }

    if (!string.IsNullOrEmpty(headers["Cache-Control"]))
    { 
        return CacheControlStrategy(response).IsValid();
    }

    if (!string.IsNullOrEmpty(headers["Expires"]))
    {
        return new ExpiresStrategy(response).IsValid();
    }

    if (string.IsNullOrEmpty(headers["Last-Modified"]))
        throw new ArgumentException("no caching strategies found", nameof(headers));

    return new RevalidateStrategy(response).IsValid();
}
```

Voor performance is gekeken naar hoe het beste omgegaan kan worden met streams. Hieronder is een voorbeeld te zien met hoe de `CopyToAsync` methode wordt gebruikt om van de ene naar de andere stream te schrijven. Hierdoor hoeft niet de volledige content in het geheugen te staan. Hetzelfde is terug te vinden bij het maken van een extern request naar het internet. Dan wordt de eventuele body van het request doorgezet naar het externe request.

```csharp
//ResponseHandler.cs
Stream exResponseStream = externalRes.GetResponseStream();

if (exResponseStream.CanSeek)
{
    exResponseStream.Position = 0;
}

await exResponseStream.CopyToAsync(response.OutputStream, config.BufferSize);
```

Een nadeel van de huidige implementatie is dat er veel model wrappers zijn geschreven om om de limitaties heen te komen. Hierdoor is een groot deel aan documentatie verloren gegaan. Daarnaast is de er geen documentatie geschreven voor de code, de mogelijkheid tot documenteren zit echter wel ingebouwd. Een voorbeeld van een Wrapper is het `ListenerRequest`.

```csharp
//ListenerRequest.cs
public class ListenerRequest : IListenerRequest
{
  private readonly HttpListenerRequest _request;
  public WebHeaderCollection Headers { get; }


  public ListenerRequest(HttpListenerRequest request)
  {
      _request = request;
      Headers = request.Headers.ToWebHeaderCollection();
  }

  public Guid RequestTraceIdentifier => _request.RequestTraceIdentifier;

  public Uri RequestUri => _request.Url;

  public Uri Url => _request.Url;

  public string RawUrl => _request.RawUrl;

  public string HttpMethod => _request.HttpMethod;

  public string UserAgent => _request.UserAgent;

  public long ContentLength64 => _request.ContentLength64;

  public Stream InputStream => _request.InputStream;
}
```

Ook is er een verbeterpunt voor in de toekomst om Dependency Injection en Middleware te kunnen gebruiken. De onderhoudtbaarheid van de request flow komt dit zeer ten goede.

Voor het design zou ik de volgorde van filtering aanpassen. Ik zou de Content en URL blocking naar voren halen om zo onnodig werkt te doen als de URL uiteindelijk geblokkeerd wordt. Daarnaast zou ik ook de request privacy filter verplaatsen naar na de aanvraag op de cache. Dit niet gedaan om het overzicht in de code te behouden. Dit zou wel goed toepasbaar zijn als er een Middleware implementatie zou zijn.

Door het implementeren van de proxy aan de hand van `HttpListener` is wel de mogelijkheid tot het implementeren van `HTTPS` vervallen. Voor `HTTPS` mag het request namelijk niet uitgepakt worden. Is dit wel het geval dan zal de client een certificaat van de proxy moeten installeren. Ook is er meer controle over het HTTP bericht. Een voorbeeld hiervan is het aanpassen van de `Server` header. Er is wel een alternatief om de `Server` header aan te passen, maar dit betekent dat er zelf een `HttpListener` geïmplementeerd moet worden.

Als laatste verbeterpunt is de response latency wanneer een request ook in de cache opgeslagen moet worden. Dan wordt deze eerst opgeslagen en daarna uitgelezen vanuit de cache. Om dit te verbeteren kan je de stream één maal uitlezen en dan tegelijkertijd wegschrijven naar twee streams.

```csharp
// cacheManager.cs
cacheStore.Add(request.RawUrl, await webResponse.ToResponseCache());
return _cacheStore.GetCache(request.RawUrl);
```

Ik heb het wegschrijven naar twee streams wel voorbereid, maar om complexiteit tegen te gaan niet geïmplementeerd.

```csharp
//RequestExtensions.cs
private static async Task CopyToMultipleAsync(this Stream sourceStream, int bufferSize,
    IReadOnlyCollection<Stream> streams)
{
    var buffer = ArrayPool<byte>.Shared.Rent(bufferSize);
    while (true)
    {
        var bytesRead = await sourceStream.ReadAsync(new Memory<byte>(buffer)).ConfigureAwait(false);
        if (bytesRead == 0) break;

        Task.WaitAll(streams.Select(stream => Task.Run(() =>
                stream.WriteAsync(new ReadOnlyMemory<byte>(buffer, 0, bytesRead)).ConfigureAwait(false)))
            .Cast<Task>().ToArray());
    }
}
```

## 5. Test cases

Om te kijken of het project aan de gestelde functionele en technische eisen voldoet kan dit getest worden door de volgende cases. Tijdens de test moeten geen andere functionaliteit aangevinkt worden tenzij anders beschreven. Graag zou ik in deze test cases ook de HTTP/1.0 compliance willen aantonen. Een manier om dit te doen is een Technical Compliance Kit (TCK). Helaas is deze online niet gevonden en zou ik dit als aanraden willen geven voor dit project in de toekomst. Als ieder project een klein deel van de TCK maakt kan iedereen hiertegen zijn proxy testen.

### 5.1. Caching

Caching kan aangezet worden door `Enable Caching` aan te vinken. Er wordt dan naar de cache headers gekeken of het http bericht gecached kan worden. Door de `Enable forced timeout` te gebruiken kan de expire tijd verlengt of verkort worden.

#### 5.1.1. Caching

Caching kan getest worden door eerst naar een website te browsen. Bij het browsen naar `http://nots-proxy.gitlab.com/http1_1_website/microschape.html` is reponse tijd tussen 200-400ms. Caching werkt als na de tweede keer laden van deze website op `localhost` de response tijd tussen de 0-5ms ligt.

#### 5.1.2. Cache Timeout

Het testen van de cache timeout kan op twee manieren. De eerst is het wachten totdat de cache header tijd verstrijkt. Bij de bovenstaand website is dan 600 seconde. De test slaagt als de response tijd na de 600 seconde bij de eerste keer laden weer tussen de 200-400ms ligt. Bij de tweede keer laden zal het request weer gecached zijn.

De tweede manier van testen in het toepassen van de `Forced Timeout`. Om deze functie te gebruiken moet de tijd ingesteld worden en de checkbox `Enable forced timeout` aan staan. Deze functie overruled alle cache headers. door de timeout op 10 te zetten zal de cache na tien seconde invalide zijn. De response tijd zal dan weer tussen de 200-400ms liggen bij het eerst request.

### 5.2. Filter ongewenste content

Er zijn twee filter om uit te kiezen. De Image filter en Block hosts filter. Deze kunnen aan en uit gezet worden door op de checkbox te klikken.

De Image filter vervangt alle afbeeldingen door een 1x1 pixel afbeelding. Dit kan getest worden door via de proxy naar `http://nots-proxy.gitlab.io/http1_1_website/microscape.html` te browsen. Door de css wordt de achtergrond van een afbeelding zwart als deze niet geladen kan worden of kleiner is dan het verwachte formaat. Het verwachte resultaat is dat na het inschakelen van de Image filter deze blokken zwart worden.

Daarnaast is er de Block hosts filter. Deze filter blokkeert bepaalde hosts. Door dit aan te zetten blokeert het `platform.twitter.com`. De verwachting is dat als er naar deze url gesurft wordt er een `502 Bad Gateway` terug komt met de tekst `Url blocked`.

### 5.4. Identiteit

Onder identiteit valt de `Protect Privacy` optie en `Enable Basic Auth`. Bij privacy worden gevoelige headers verwijderd zodat deze niet naar het internet toe verstuurd wordt. Basic auth zorgt ervoor dat iemand zich tegen de proxy moet identificeren.

#### 5.4.1. Protect Privacy

Om dit te testen kan er een request worden verstuurd door middel van [Postman](https://www.postman.com/). In dit request kunnen verschillende headers toegevoegd worden. Om te testen kunnen één of meerdere van de al eerder [genoemd](#1-architecture) privacy gevoelige headers toegevoegd worden. Daarnaast is het handig om `Logging` aan te hebben staan.

Als het request gemaakt wordt en logging staat aan dan worden de request headers getoond in de log. Naar verwachting zouden dan de privacy gevoeligde headers hieruit verwijderd moeten zijn.

#### 5.4.2. Basic Auth

Om Basic authenticatie te testen moet dit aangezet worden. De manier waarop `HttpListener` werkt kan dit alleen voorafgaand aan het opstarten. Daarom moet deze optie aangevinkt worden voordat de proxy gestart wordt.

Door nu de browsen naar een website zal er in de browser een popup verschijnen waar de gebruikersnaam en wachtwoord ingevuld moet worden, `admin/admin`. Na het invoeren van de gegevens wordt de website geladen.

### 5.5. Buffersize

Om Buffersize te testen kan buffersize veranderd worden. Standaard staat de buffersize op `81920`. Door dit te veranderen naar `1` zal de laattijd van de website langer zijn. De test slaagt als de laadtijd van `http://nots-proxy.com/http1_1_website/microscape.html` groter is dan 9000ms. Bij het installen van een buffersize van `0` wordt de buffersize gereset naar `81920`.

### 5.6 Geschikt voor tekst en binaire data

Om te bekijken of de proxy geschikt is voor beide type data kan er gesurfd worden naar de test website `http://nots-proxy.com/http1_1_website/microscape.html`. Deze website laad zowel `html` als `gif`. Als de website volledig geladen wordt dan is de proxy geschikt voor beide type data.
