using Shared.Exceptions;
using Shared.Extensions;
using Shared.Logging;
using Shared.Models;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
namespace Shared
{
    public class ResponseHandler
    {
        public async void HandleResponse(
            HttpListenerResponse response,
            IResponse externalRes,
            IListenerRequest listenerRequest,
            ProxyConfig config
            )
        {
            response.Cookies = externalRes.Cookies;
            response.AddHeaders(externalRes);
            response.StatusCode = (int)externalRes.StatusCode;

            try
            {
                Stream exResponseStream = externalRes.GetResponseStream();

                if (exResponseStream.CanSeek)
                {
                    exResponseStream.Position = 0;
                }

                await exResponseStream.CopyToAsync(response.OutputStream, config.BufferSize);

                response.Close();
            }
            catch (Exception e)
            {
                Logger.Error(e, listenerRequest.RequestTraceIdentifier, config.IsLoggingEnabled);
            }
        }

        public async Task HandleCustomResponseError(HttpListenerResponse response, WebResponseException exception,
            IListenerRequest listenerRequest, ProxyConfig config)
        {
            try
            {
                Logger.Info(exception.Message, exception.TraceId, config.IsLoggingEnabled);
                response.StatusCode = (int)exception.StatusCode;
                await exception.Message.ToStream().CopyToAsync(response.OutputStream, config.BufferSize);
                response.Close();
            }
            catch (Exception e)
            {
                Logger.Error(e, listenerRequest.RequestTraceIdentifier, config.IsLoggingEnabled);
            }
        }

        public async Task HandleResponseError(HttpListenerResponse response, WebException webException,
            IListenerRequest listenerRequest, ProxyConfig config)
        {
            var externalResponse = (HttpWebResponse)webException.Response;

            if (externalResponse == null)
            {
                response.StatusCode = 500;
                var message = Encoding.UTF8.GetBytes(webException.Message);
                response.OutputStream.Write(message, 0, message.Length);
                response.Close();
                return;
            }

            var exResponse = new Response(externalResponse);

            try
            {
                response.StatusCode = (int)externalResponse.StatusCode;
                response.AddHeaders(exResponse);
                response.Cookies = externalResponse.Cookies;

                var externalStream = externalResponse.GetResponseStream();
                if (externalStream != null) await externalStream.CopyToAsync(response.OutputStream, config.BufferSize);

                response.Close();
            }
            catch (Exception e)
            {
                response.Close();
                Logger.Error(e, listenerRequest.RequestTraceIdentifier, config.IsLoggingEnabled);
            }
        }
    }
}