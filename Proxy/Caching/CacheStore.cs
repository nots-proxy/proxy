using Shared.Models;
using System;
using System.Collections.Concurrent;
using System.Linq;

namespace Shared.Caching
{
    public sealed class CacheStore
    {
        private static ConcurrentDictionary<string, IResponse> _cache;
        private static ConcurrentDictionary<string, DateTime> _cacheQueue;
        private const int MaxCacheSize = 1000;

        public CacheStore()
        {
            CreateDictionary();
        }

        private void CreateDictionary()
        {
            if (_cache == null)
                _cache = new ConcurrentDictionary<string, IResponse>();
            if (_cacheQueue == null)
                _cacheQueue = new ConcurrentDictionary<string, DateTime>();
        }

        public bool ContainsKey(string key) => _cache.ContainsKey(key);

        public int GetSize() => _cache.Count;

        public void Add(string key, IResponse response)
        {
            if (_cacheQueue.Count >= MaxCacheSize)
            {
                RemoveFirstItem();
            }
            _cacheQueue.TryAdd(key, DateTime.Now);
            _cache.TryAdd(key, response);
        }

        public void Remove(string key)
        {
            var isRemoved = _cache.TryRemove(key, out _);
            if (isRemoved)
            {
                _cacheQueue.TryRemove(key, out _);
            }
        }

        private void RemoveFirstItem()
        {
            var cacheQueueKey = _cacheQueue.OrderBy(item => item.Value).FirstOrDefault().Key;
            Remove(cacheQueueKey);
        }

        public IResponse GetCache(string key)
        {
            _cache.TryGetValue(key, out var response);
            return response;
        }
    }
}