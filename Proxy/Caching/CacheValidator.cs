using System;
using Shared.Caching.Strategy;
using Shared.Models;

namespace Shared.Caching
{
    public class CacheValidator
    {
        public bool IsValid(IResponse response, ProxyConfig config)
        {
            var headers = response.Headers;

            if (config.IsForcedTimeout)
            {
                return new ForcedStrategy(response, config.ForcedTimeout).IsValid();
            }

            if (!string.IsNullOrEmpty(headers["Cache-Control"]))
            { 
                return CacheControlStrategy(response).IsValid();
            }

            if (!string.IsNullOrEmpty(headers["Expires"]))
            {
                return new ExpiresStrategy(response).IsValid();
            }

            if (string.IsNullOrEmpty(headers["Last-Modified"]))
                throw new ArgumentException("no caching strategies found", nameof(headers));

            return new RevalidateStrategy(response).IsValid();
        }

        private ICachingStrategy CacheControlStrategy(IResponse response)
        {
            var ccHeader = response.Headers["Cache-Control"];
            if (ccHeader.Contains("immutable"))
            {
                return new ImmutableStrategy();
            }

            if (ccHeader.Contains("must-revalidate") || ccHeader.Contains("proxy-revalidate"))
            {
                // TODO: There needs to be some real date here.
                return new RevalidateStrategy(response);
            }
                
            // s-maxage overrides max-age and expires. Always use s-maxage before max-age
            if (ccHeader.Contains("s-maxage") || ccHeader.Contains("max-age"))
            {
                return new ExpiresStrategy(response);
            }
            // if (ccHeader.Contains("s-maxage"))
            // {
            //     var date = DateTime.Parse(response.Headers.Get("Date"));
            //     var sMaxAgeString = ccHeader.Split(",").First(s => s.Contains("s-maxage")).Split("=")
            //         .Last();
            //     var sMaxAge = int.Parse(sMaxAgeString);
            //     return new ExpiresStrategy(date.AddSeconds(sMaxAge));
            // }
            //
            // if (ccHeader.Contains("max-age"))
            // {
            //     var date = DateTime.Parse(response.Headers.Get("Date"));
            //     var maxAgeString = ccHeader.Split(",").First(s => s.Contains("max-age")).Split("=")
            //         .Last();
            //     var maxAge = int.Parse(maxAgeString);
            //     return new ExpiresStrategy(date.AddSeconds(maxAge));
            // }

            throw new Exception("Cache-Control headers are invalid");
        }
    }
}