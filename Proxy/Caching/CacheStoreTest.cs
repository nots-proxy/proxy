using System.IO;
using Moq;
using Shared.Extensions;
using Shared.Models;
using Xunit;

namespace Shared.Caching
{
    public class CacheStoreTest
    {
        [Fact]
        public void AddAndRetrieveItem()
        {
            // Arrange
            var cache = new CacheStore();

            const string key = "AddAndRetrieveItemTest";
            const string value = "test";

            var response = new Mock<IResponse>();
            response.Setup(r => r.GetResponseStream()).Returns(value.ToStream());

            // Act
            cache.Add(key, response.Object);

            // Assert
            var res = cache.GetCache(key);
            var text = new StreamReader(res.GetResponseStream()).ReadToEnd();
            Assert.Equal(value, text);
        }

        [Fact]
        public void TestSingleCache()
        {
            // Arrange
            var cache = new CacheStore();
            var cache2 = new CacheStore();

            const string key1 = "TestSingleCacheTest1";
            const string key2 = "TestSingleCacheTest2";
            const string value = "test";

            var response1 = new Mock<IResponse>();
            response1.Setup(r => r.GetResponseStream()).Returns(value.ToStream());

            var response2 = new Mock<IResponse>();
            response2.Setup(r => r.GetResponseStream()).Returns(value.ToStream());


            // Act
            cache.Add(key1, response1.Object);
            cache2.Add(key2, response2.Object);

            // Assert
            var res1 = cache.GetCache(key1);
            var res2 = cache.GetCache(key2);

            Assert.NotNull(res1);
            Assert.NotNull(res2);

            Assert.Equal(2, cache.GetSize());
        }
    }
}