using System.Collections.Specialized;

namespace Shared.Caching
{
    public class ClientHeaderValidator
    {
        public bool AcceptOnlyCached(NameValueCollection headers)
        {
            var ifNoneMatchHeader = headers.Get("If-None-Match");
            if (ifNoneMatchHeader != null) return false;

            var ccHeader = headers.Get("Cache-Header");
            return ccHeader != null && ccHeader.Contains("only-if-cached");
        }
    }
}