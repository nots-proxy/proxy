namespace Shared.Caching.Strategy
{
    public class ImmutableStrategy : ICachingStrategy
    {
        public bool IsValid() => true;
    }
}