namespace Shared.Caching.Strategy
{
    public interface ICachingStrategy
    {
        bool IsValid();
    }
}