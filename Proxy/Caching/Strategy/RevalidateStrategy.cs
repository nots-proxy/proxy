using Shared.Models;
using Shared.Web;
using System.Net;

namespace Shared.Caching.Strategy
{
    public class RevalidateStrategy : ICachingStrategy
    {
        private readonly IRequest _request;

        public RevalidateStrategy(IResponse response)
        {
            // _request = WebRequest.CreateHttp(url);
            // TODO: fix that method can change. GET and HEAD requests can be used;
            // _request.Method = "GET";
            // _request.UserAgent = "MOZILLA/5.0 (WINDOWS NT 6.1; WOW64) APPLEWEBKIT/537.1 (KHTML, LIKE GECKO) CHROME/21.0.1180.75 SAFARI/537.1";
            // _request.Headers.Add("If-Modified-Since", header);
            var wManager = new WebManager();
            _request = wManager.BuildValidateRequest(response);
        }

        public RevalidateStrategy(IResponse response, IWebManager manager)
        {
            _request = manager.BuildValidateRequest(response);
        }

        public bool IsValid()
        {
            // TODO: Heuristic freshness checking
            // expirationTime = responseTime + freshnessLifetime - currentAge
            // TODO: Last-modified check
            var res = _request.GetResponse().Result;

            if (res.StatusCode == HttpStatusCode.NotModified)
                return true;
            return false;
        }
    }
}