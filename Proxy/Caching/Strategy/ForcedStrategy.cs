﻿using Shared.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Caching.Strategy
{
    class ForcedStrategy : ICachingStrategy
    {
        private readonly DateTime expireDate;

        public ForcedStrategy(IResponse response, int forcedTimeout)
        {
            var dHeader = response.Headers.Get("Date");
            var responseDate = DateTime.Parse(dHeader);
            expireDate = responseDate.AddSeconds(forcedTimeout);
        }

        public bool IsValid()
        {
            return expireDate >= DateTime.Now;
        }
    }
}
