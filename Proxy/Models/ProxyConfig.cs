﻿using Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    //public class ProxyConfig : BindableBase
    //{
    //    public int Port { get; set; }
    //    public bool Stop { get; set; }
    //    public bool IsContentBlockingEnabled { get; set; }
    //    public bool IsUrlBlockingEnabled { get; set; }
    //    public bool IsPrivacyEnabled { get; set; }
    //    public bool IsBasicAuthEnabled { get; set; }
    //    public bool IsLoggingEnabled { get; set; }
    //    public class Credentials
    //    {
    //        public string Username { get; set; }
    //        public string Password { get; set; }
    //    }
    //    public Credentials AuthCredentials { get; set; }
    //    public bool IsCachingEnabled { get; set; }
    //    public int BufferSize { get; set; }
    //    public List<string> BlockList { get; set; }
    //}

    public sealed class ProxyConfig : BindableBase
    {
        public ProxyConfig(Credentials credentials = null)
        {
            authCredentials = credentials ?? new Credentials();
            blockList = new List<string>();
        }
        private int port = 8080;
        private bool stop;
        private bool isRunning;
        private bool isContentBlockingEnabled;
        private bool isUrlBlockingEnabled;
        private bool isPrivacyEnabled;
        private bool isBasicAuthEnabled;
        private bool isLoggingEnabled;
        private Credentials authCredentials;
        private bool isCachingEnabled;
        private int bufferSize = 81920;
        private List<string> blockList;
        private bool isForcedTimeout;
        private int forcedTimeout;

        public int Port { get => port; set => Set(ref port, value); }
        public bool Stop { get => stop; set => Set(ref stop, value); }
        public bool IsRunning { get => isRunning; set => Set(ref isRunning, value); }
        public bool IsContentBlockingEnabled { get => isContentBlockingEnabled; set => Set(ref isContentBlockingEnabled, value); }
        public bool IsUrlBlockingEnabled { get => isUrlBlockingEnabled; set => Set(ref isUrlBlockingEnabled, value); }
        public bool IsPrivacyEnabled { get => isPrivacyEnabled; set => Set(ref isPrivacyEnabled, value); }
        public bool IsBasicAuthEnabled { get => isBasicAuthEnabled; set => Set(ref isBasicAuthEnabled, value); }
        public bool IsLoggingEnabled { get => isLoggingEnabled; set => Set(ref isLoggingEnabled, value); }
        public Credentials AuthCredentials { get => authCredentials; set => Set(ref authCredentials, value); }
        public bool IsCachingEnabled { get => isCachingEnabled; set => Set(ref isCachingEnabled, value); }
        public int BufferSize
        {
            get => bufferSize; 
            set
            {
                if (value <= 0) value = 81920;
                Set(ref bufferSize, value);
            }
        }
        public List<string> BlockList { get => blockList; set => Set(ref blockList, value); }
        public bool IsForcedTimeout { get => isForcedTimeout; set => Set(ref isForcedTimeout, value); }
        public int ForcedTimeout { get => forcedTimeout; set => Set(ref forcedTimeout, value); }
    }

    public sealed class Credentials : BindableBase
    {
        private string username = "admin";
        private string password = "password";
        public string Username { get => username; set => Set(ref username, value); }
        public string Password { get => password; set => Set(ref password, value); }
    }
}

