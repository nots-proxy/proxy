using System;
using System.IO;
using System.Net;

namespace Shared.Models
{
    public class ResponseFilter : IResponse
    {
        public ResponseFilter(CookieCollection cookies, WebHeaderCollection headers, Stream stream, HttpStatusCode statusCode)
        {
            Cookies = cookies;
            Headers = headers;
            Stream = stream;
            StatusCode = statusCode;
        }

        private Stream Stream { get; }
        public CookieCollection Cookies { get; }
        public WebHeaderCollection Headers { get; }
        public HttpStatusCode StatusCode { get; set; }
        public Uri ResponseUri { get; set; }
        public Stream GetResponseStream() => Stream;
    }
}