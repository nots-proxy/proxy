using Shared.Extensions;
using System;
using System.IO;
using System.Net;

namespace Shared.Models
{
    public interface IListenerRequest
    {
        Uri RequestUri { get; }
        string RawUrl { get; }
        Uri Url { get; }
        string HttpMethod { get; }
        string UserAgent { get; }
        long ContentLength64 { get; }
        Stream InputStream { get; }
        WebHeaderCollection Headers { get; }
        Guid RequestTraceIdentifier { get; }
    }

    public class ListenerRequest : IListenerRequest
    {
        private readonly HttpListenerRequest _request;
        public WebHeaderCollection Headers { get; }


        public ListenerRequest(HttpListenerRequest request)
        {
            _request = request;
            Headers = request.Headers.ToWebHeaderCollection();
        }

        public Guid RequestTraceIdentifier => _request.RequestTraceIdentifier;

        public Uri RequestUri => _request.Url;

        public Uri Url => _request.Url;

        public string RawUrl => _request.RawUrl;

        public string HttpMethod => _request.HttpMethod;

        public string UserAgent => _request.UserAgent;

        public long ContentLength64 => _request.ContentLength64;

        public Stream InputStream => _request.InputStream;
    }
}