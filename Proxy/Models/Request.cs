using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Shared.Models
{
    public class Request : IRequest
    {

        private readonly HttpWebRequest _request;

        public Request(string url)
        {
            _request = WebRequest.CreateHttp(url);
        }

        public Uri RequestUri => _request.RequestUri;

        public void OverrideHeaders(WebHeaderCollection headers)
        {
            _request.Headers = headers;
        }

        public void AddHeader(HttpRequestHeader header, string value)
        {
            if (header == HttpRequestHeader.IfModifiedSince)
            {
                _request.IfModifiedSince = DateTime.Parse(value);
                return;
            }
            _request.Headers.Set(header, value);
        }

        public async void AddBody(Stream body)
        {
            await body.CopyToAsync(await _request.GetRequestStreamAsync());
        }

        public void AddHttpMethod(HttpMethod httpMethod)
        {
            _request.Method = httpMethod.Method;
        }

        public WebHeaderCollection Headers => _request.Headers;

        public async Task<IResponse> GetResponse()
        {
            var response = (HttpWebResponse)await _request.GetResponseAsync();
            return new Response(response);
        }
    }
}