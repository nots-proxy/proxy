using System;
using System.IO;
using System.Net;

namespace Shared.Models
{
    public interface IResponse
    {
        Uri ResponseUri { get; }
        CookieCollection Cookies { get; }
        WebHeaderCollection Headers { get; }
        Stream GetResponseStream();
        HttpStatusCode StatusCode { get; }
    }
}