using System;
using System.Net;
using System.Threading.Tasks;

namespace Shared.Models
{
    public interface IRequest
    {
        Uri RequestUri { get; }
        WebHeaderCollection Headers { get; }

        Task<IResponse> GetResponse();
    }
}