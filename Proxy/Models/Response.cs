using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Shared.Models
{
    public class Response : IResponse
    {
        private readonly HttpWebResponse _response;

        public Response(HttpWebResponse response)
        {
            _response = response;
        }

        public async Task<ResponseCache> ToResponseCache()
        {
            return await ResponseCache.From(_response);
        }

        public Uri ResponseUri => _response.ResponseUri;
        public CookieCollection Cookies => _response.Cookies;
        public WebHeaderCollection Headers => _response.Headers;
        public virtual Stream GetResponseStream() => _response.GetResponseStream();
        public HttpStatusCode StatusCode => _response.StatusCode;

        public bool isError()
        {
            var code = (int)_response.StatusCode;
            if (code >= 500) return true;
            return false;
        }
    }
}