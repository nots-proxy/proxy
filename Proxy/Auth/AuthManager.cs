using Shared.Models;
using System.Net;
using System.Text;
using static Shared.ProxyConfig;

namespace Shared.Auth
{
    public class AuthManager
    {
        public bool IsAuthorized(HttpListenerBasicIdentity identity, Credentials credentials)
        {
            return identity.Name == credentials.Username && identity.Password == credentials.Password;
        }

        public void HandleUnauthorized(HttpListenerResponse response)
        {
            response.StatusCode = 401;
            response.AddHeader("WWW-authenticate", "Basic Realm=\"Custom proxy\"");
            var message = new UTF8Encoding().GetBytes("Access denied");
            response.ContentLength64 = message.Length;
            response.OutputStream.Write(message, 0, message.Length);
            response.Close();
        }

        public void RemoveAuthHeaders(IListenerRequest request)
        {
            request.Headers.Remove("Authorization");
            request.Headers.Remove("Upgrade-Insecure-Requests");
        }
    }
}