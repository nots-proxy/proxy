﻿using System.Threading.Tasks;
using System.Net;
using Shared.Models;
using Shared.Auth;
using Shared.Privacy;
using Shared.Filter;
using Shared.Caching;
using System;
using Shared;
using Shared.Exceptions;
using Shared.Logging;

namespace shared
{
    class RequestHandler
    {
        public async void HandleRequestAsync(HttpListenerContext context, ProxyConfig config)
        {
            var request = new ListenerRequest(context.Request);
            var response = context.Response;

            LogIncomingRequest(request, config.IsLoggingEnabled);

            if (context.User?.Identity != null)
            {
                var authManager = new AuthManager();
                if (!authManager.IsAuthorized((HttpListenerBasicIdentity) context.User.Identity, config.AuthCredentials))
                {
                    authManager.HandleUnauthorized(response);
                    return;
                }
                authManager.RemoveAuthHeaders(request);
            }

            var responseHandler = new ResponseHandler();
            try
            {
                var extRes = await FetchRequest(request, config);
                responseHandler.HandleResponse(response, extRes, request, config);
            }
            catch (WebResponseException e)
            {
                await responseHandler.HandleCustomResponseError(response, e, request, config);
            }
            catch (WebException e)
            {
                await responseHandler.HandleResponseError(response, e, request, config);
            }
            catch (Exception e)
            {
                Logger.Error(e, request.RequestTraceIdentifier, config.IsLoggingEnabled);
                response.StatusCode = 500;
                response.Close();
            }
        }

        private async Task<IResponse> FetchRequest(IListenerRequest request, ProxyConfig config)
        {
            new PrivacyManager().RemoveClientHeaders(request);
            var res = new FilterManager().Filter(request, config);
            if (res != null) return res;
            return await new CacheManager().GetRequest(request, config);
        }

        private void LogIncomingRequest(IListenerRequest request, bool isLogging)
        {
            var message = $"{request.RawUrl}: {request.Headers}";
            Logger.Info(message, request.RequestTraceIdentifier, isLogging);
        }
    }
}
