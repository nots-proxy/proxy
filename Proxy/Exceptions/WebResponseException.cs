using System;
using System.Net;

namespace Shared.Exceptions
{
    public class WebResponseException : Exception
    {
        public HttpStatusCode StatusCode { get; }
        public Guid TraceId { get; }

        public WebResponseException(HttpStatusCode statusCode, string message) : base(message)
        {
            StatusCode = statusCode;
        }

        public WebResponseException(string message) : base(message) { }
        public WebResponseException(string message, Exception inner) : base(message, inner) { }

        public WebResponseException(HttpStatusCode statusCode, string message, Guid traceId) : base(message)
        {
            StatusCode = statusCode;
            TraceId = traceId;
        }
    }
}