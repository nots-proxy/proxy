using System;
using System.Linq;

namespace Shared.Extensions
{
    public static class UriExtension
    {
        public static string GetFileExtensionFromUrl(this Uri uri)
        {
            var url = uri.AbsoluteUri;
            url = url.Split('?')[0];
            url = url.Split('/').Last();
            return url.Contains('.') ? url.Substring(url.LastIndexOf('.')) : "";
        }
    }
}