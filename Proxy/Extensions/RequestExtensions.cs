using Shared.Models;
using System.Collections.Specialized;
using System.Linq;
using System.Net;

namespace Shared.Extensions
{
    public static class RequestExtension
    {
        public static void AddHeaders(this HttpWebRequest request, NameValueCollection headers)
        {
            var headerWithValues =
                request.Headers.AllKeys.SelectMany(request.Headers.GetValues, (k, v) => new { key = k, value = v });

            foreach (var header in headerWithValues)
            {
                if (header.key == "User-Agent")
                {
                    request.UserAgent = header.value;
                } else
                {
                    request.Headers.Add(header.key, header.value);
                }
            }
        }

        public static void AddHeaders(this HttpListenerResponse response, IResponse exResponse)
        {
            var headers = exResponse.Headers;

            var headerWithValues =
                headers.AllKeys.SelectMany(headers.GetValues, (k, v) => new { key = k, value = v });

            foreach (var header in headerWithValues)
            {
                if (header.key == "Content-Length")
                {
                    //response.ContentLength64 = long.Parse(header.value);
                }
                else if(header.key == "Content-Type")
                {
                    response.ContentType = header.value;
                }
                else if(header.key == "Content-Encoding")
                {
                    //response.ContentEncoding =  header.value;
                }
                else if(header.key == "Location")
                {
                    response.RedirectLocation = header.value;
                }
                else if(header.key == "Transfer-Encoding")
                {
                    if (header.value == "chuncked")
                    {
                        response.SendChunked = true;
                    }
                    else
                    {
                        //response.ContentEncoding = System.Text.Encoding.UTF8;
                    }
                }
                else if(header.key == "Keep-Alive")
                {
                    response.KeepAlive = true;
                }
                else
                {
                    response.Headers.Add(header.key, header.value);
                }
            }
        }

        private static void Add(WebHeaderCollection toHeaders, NameValueCollection fromHeaders)
        {
            
        }
    }

    // public static class StreamExtension
    // {
    //     public static Task CopyToMultipleAsync(this Stream sourceStream, List<Stream> streams)
    //     {
    //         var bufferSize = GetBufferSize(sourceStream);
    //         return CopyToMultipleAsync(sourceStream, bufferSize, streams);
    //     }
    //
    //     private static async Task CopyToMultipleAsync(this Stream sourceStream, int bufferSize,
    //         IReadOnlyCollection<Stream> streams)
    //     {
    //         var buffer = ArrayPool<byte>.Shared.Rent(bufferSize);
    //         while (true)
    //         {
    //             var bytesRead = await sourceStream.ReadAsync(new Memory<byte>(buffer)).ConfigureAwait(false);
    //             if (bytesRead == 0) break;
    //
    //             Task.WaitAll(streams.Select(stream => Task.Run(() =>
    //                     stream.WriteAsync(new ReadOnlyMemory<byte>(buffer, 0, bytesRead)).ConfigureAwait(false)))
    //                 .Cast<Task>().ToArray());
    //         }
    //     }
    //
    //     private static int GetBufferSize(Stream stream)
    //     {
    //         var bufferSize = 81920;
    //
    //         if (stream.CanSeek)
    //         {
    //             var length = stream.Length;
    //             var position = stream.Position;
    //
    //             if (length <= position)
    //             {
    //                 bufferSize = 1;
    //             }
    //             else
    //             {
    //                 var remaining = length - position;
    //                 if (remaining > 0)
    //                 {
    //                     bufferSize = (int) Math.Min(bufferSize, remaining);
    //                 }
    //             }
    //         }
    //
    //         return bufferSize;
    //     }
    // }
}
