using System.Collections.Specialized;
using System.Linq;
using System.Net;

namespace Shared.Extensions
{
    public static class NameValueCollectionExtension
    {
        public static WebHeaderCollection ToWebHeaderCollection(this NameValueCollection headers)
        {
            return headers.AllKeys.Aggregate(
                new WebHeaderCollection(),
                (headerCollection, headerName) =>
                {
                    headerCollection.Add(headerName, headers[headerName]);
                    return headerCollection;
                }
            );
        }
    }
}