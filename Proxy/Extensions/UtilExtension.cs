using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Shared.Extensions
{
    public static class UtilExtension
    {
        public static Stream ToStream(this string s)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public static bool IsAny<T>(this IEnumerable<T> source) => source != null && source.Any();
    }
}