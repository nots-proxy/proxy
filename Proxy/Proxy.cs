﻿using shared;
using Shared.Logging;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Shared
{
    public sealed class Proxy
    {
        public ProxyConfig Config;

        private HttpListener _httpListener;

        public Proxy(ProxyConfig config)
        {
            Config = config;
        }

        public bool IsRunning
        {
            get
            {
                if (_httpListener != null && _httpListener.IsListening)
                    return true;
                return false;
            }
        }

        public void Start()
        {
            if (IsRunning)
            {
                throw new Exception("Proxy already running");
            }

            _httpListener = new HttpListener();

            var prefix = $"http://+:{Config.Port}/";
            _httpListener.Prefixes.Add(prefix);
            if (Config.IsBasicAuthEnabled)
            {
                _httpListener.AuthenticationSchemes = AuthenticationSchemes.Basic;
            }

            _httpListener.Start();

            Task.Run(() => Listen());
            Logger.Info("Proxy started", true);
        }

        public void Stop()
        { 
            if (_httpListener != null && _httpListener.IsListening)
            {
                _httpListener.Stop();
                Logger.Info("Proxy Stopped", true);
            }
        }

        private void Listen()
        {
            do
            {
                try
                {
                    var context = _httpListener.GetContext();
                    new RequestHandler().HandleRequestAsync(context, Config);
                }
                catch (Exception e)
                {
                    Logger.Error(e, Config.IsLoggingEnabled);
                }
            } while (_httpListener.IsListening);
            
        }
    }
}
