using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Shared.Extensions;
using Shared.Logging;
using Shared.Models;

namespace Shared.Web
{
    public class WebManager : IWebManager
    {
        public async Task<HttpWebRequest> BuildWebRequest(IListenerRequest request, ProxyConfig config)
        {
            Logger.Info("Building new request", request.RequestTraceIdentifier, config.IsLoggingEnabled);
            var http = WebRequest.CreateHttp(request.RawUrl);
            http.Method = request.HttpMethod;

            if (config.IsLoggingEnabled)
            {
                string headerString = string.Join(",",
                    request.Headers.AllKeys
                        .Where(key => !string.IsNullOrWhiteSpace(request.Headers[key]))
                        .Select(key => string.Format("{0}:{1}", key, request.Headers[key]))
                    );
                Logger.Info(headerString, request.RequestTraceIdentifier, config.IsLoggingEnabled);
            }

            http.UserAgent = request.UserAgent;
            http.AddHeaders(request.Headers);

            if (request.ContentLength64 <= 0) return http;

            var httpRequestStream = http.GetRequestStream();
            await request.InputStream.CopyToAsync(httpRequestStream, config.BufferSize);

            return http;
        }

        public IRequest BuildValidateRequest(IResponse response)
        {
            var request = new Request(response.ResponseUri.ToString());
            request.AddHttpMethod(HttpMethod.Get);
            request.AddHeader(HttpRequestHeader.IfModifiedSince, response.Headers.Get("Date"));
            return request;
        }
    }
}