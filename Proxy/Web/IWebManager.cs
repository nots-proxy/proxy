using System.Net;
using System.Threading.Tasks;
using Shared.Models;

namespace Shared.Web
{
    public interface IWebManager
    {
        Task<HttpWebRequest> BuildWebRequest(IListenerRequest request, ProxyConfig config);
        IRequest BuildValidateRequest(IResponse requestResponse);
    }
}