using Shared.Exceptions;
using Shared.Extensions;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace backend.Filtering.Strategy
{
    public class UrlFilterStrategy
    {
        public IResponse Filter(Uri uri, List<string> blockList)
        {
            if (blockList.IsAny() && blockList.Any(s => s == uri.Host))
            {
                throw new WebResponseException(HttpStatusCode.BadGateway, "Url blocked");
            }

            return null;
        }
    }
}