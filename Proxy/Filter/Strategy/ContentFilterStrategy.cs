using Shared.Extensions;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;

namespace Shared.Filter.Strategy
{
    public class ContentFilterStrategy
    {
        private readonly List<string> _mimeTypes = new List<string> { ".gif", ".jpg", ".png" };
        private readonly Bitmap _bitmap = new Bitmap(1, 1);

        public IResponse Filter(Uri uri)
        {
            var extension = uri.GetFileExtensionFromUrl();
            if (!_mimeTypes.Contains(extension)) return null;

            var stream = new MemoryStream();
            switch (extension)
            {
                case ".gif":
                    _bitmap.Save(stream, ImageFormat.Gif);
                    break;
                case "jpg":
                    _bitmap.Save(stream, ImageFormat.Jpeg);
                    break;
                case ".png":
                    _bitmap.Save(stream, ImageFormat.Png);
                    break;
            }

            return new ResponseFilter(new CookieCollection(), new WebHeaderCollection(), stream, HttpStatusCode.OK);
        }
    }
}