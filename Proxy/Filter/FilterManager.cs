using backend.Filtering.Strategy;
using Shared.Filter.Strategy;
using Shared.Logging;
using Shared.Models;
using System.Collections.Generic;

namespace Shared.Filter
{
    public class FilterManager
    {
        public IResponse Filter(IListenerRequest request, ProxyConfig config)
        {
            if (config.IsContentBlockingEnabled)
            {
                var contentFilter = new ContentFilterStrategy();
                var cfResponse = contentFilter.Filter(request.Url);
                if (cfResponse != null)
                {
                    Logger.Info("Content Blocking", request.RequestTraceIdentifier, config.IsLoggingEnabled);
                    return cfResponse;
                }
            }

            if (config.IsUrlBlockingEnabled)
            {
                var urlFilter = new UrlFilterStrategy();
                var ufResponse = urlFilter.Filter(request.Url, config.BlockList);
                if (ufResponse != null)
                {
                    Logger.Info("Url blocking", request.RequestTraceIdentifier, config.IsLoggingEnabled);
                    return ufResponse;
                }
            }

            return null;
        }
    }
}