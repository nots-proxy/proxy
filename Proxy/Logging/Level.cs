namespace Shared.Logging
{
    public enum Level
    {
        None,
        Debug,
        Info,
        Warning,
        Error
    }
}