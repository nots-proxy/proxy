﻿using System;
using System.Collections.ObjectModel;
using System.Threading;

namespace Shared.Logging
{
    public static class Logger
    {
        public static readonly ObservableCollection<LogMessage> logs = new ObservableCollection<LogMessage>();
        private static readonly SynchronizationContext context = SynchronizationContext.Current;

        public static void Error(Exception e, bool isLogging)
        {
            Log(Level.Error, e.Message, DateTime.Now, isLogging);
        }
        public static void Error(Exception e, Guid guid, bool isLogging)
        {
            Log(Level.Error, e.Message, DateTime.Now, guid, isLogging);
        }

        public static void Error(string message, Guid guid, bool isLogging)
        {
            Log(Level.Error, message, DateTime.Now, guid, isLogging);
        }

        public static void Info(string message, bool isLogging)
        {
            Log(Level.Info, message, DateTime.Now, isLogging);
        }

        public static void Info(string message, Guid guid, bool isLogging)
        {
            Log(Level.Info, message, DateTime.Now, guid, isLogging);
        }

        private static void Log(Level level, string message, DateTime dateTime, bool isLogging)
        {
            if (isLogging)
            {
                context.Send(x => logs.Add(new LogMessage(level, message, dateTime)), null);
            }
        }

        private static void Log(Level level, string message, DateTime dateTime, Guid guid, bool isLogging)
        {
            if (isLogging)
            {
                context.Send(x => logs.Add(new LogMessage(level, message, dateTime, guid)), null);
            }
        }

        public static void Clear()
        {
            logs.Clear();
        }
    }
}
