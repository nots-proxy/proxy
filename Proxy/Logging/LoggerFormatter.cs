namespace Shared.Logging
{
    internal class LoggerFormatter
    {
        public string ApplyFormat(LogMessage logMessage)
        {
            return logMessage.TraceId == null
                ? $"{logMessage.DateTime:G}: {logMessage.Level}: {logMessage.Text}"
                : $"{logMessage.DateTime:G}: {logMessage.Level}: {logMessage.TraceId} : {logMessage.Text}";
        }
    }
}